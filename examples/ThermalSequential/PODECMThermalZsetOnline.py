# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from genericROM.IO import ZsetSolutionWriter as ZSW
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import Solution as S
from genericROM.IO import PXDMFWriter as PW
from genericROM.OperatorCompressors import Thermal_transient as Th
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os



def test():


    import time
    start = time.time()


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    ##################################################
    # LOAD DATA FOR ONLINE
    ##################################################

    collectionProblemData = SIO.LoadState("collectionProblemData")
    operatorCompressionData = collectionProblemData.GetOperatorCompressionData("T")
    snapshotCorrelationOperator = SIO.LoadState("snapshotCorrelationOperator")
    reducedOrderBases = collectionProblemData.GetReducedOrderBases()


    ##################################################
    # ONLINE
    ##################################################

    folder = GetTestDataPath()+"Zset"+os.sep+"ThermalSequential"+os.sep

    inputFileName = folder + "square.inp"
    inputReader = ZIR.ZsetInputReader(inputFileName)

    meshFileName = folder + "square.geof"
    mesh = ZMR.ReadMesh(meshFileName)

    onlineProblemData = PD.ProblemData("Online")
    onlineProblemData.SetDataFolder(os.path.relpath(folder, folderHandler.scriptFolder))

    timeSequence = inputReader.ReadInputTimeSequence()

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()
    onlineProblemData.AddConstitutiveLaw(constitutiveLawsList)

    loadingList = inputReader.ConstructLoadingsList()
    onlineProblemData.AddLoading(loadingList)
    for loading in onlineProblemData.GetLoadingsForSolution("T"):
        loading.ReduceLoading(mesh, onlineProblemData, reducedOrderBases, operatorCompressionData)

    initialCondition = inputReader.ConstructInitialCondition()
    onlineProblemData.SetInitialCondition(initialCondition)

    initialCondition.ReduceInitialSnapshot(reducedOrderBases, snapshotCorrelationOperator)

    durationInitialization = time.time() - start

    start = time.time()
    onlineReducedCoordinates = Th.ComputeOnline(onlineProblemData, timeSequence, operatorCompressionData, 1.e-5)

    print(">>>> DURATION INITIALIZATION =", durationInitialization)
    print(">>>> DURATION ONLINE         =", time.time() - start)

    onlineSolution = S.Solution("T", 1, mesh.GetNumberOfNodes(), primality = True)
    onlineSolution.SetReducedCoordinates(onlineReducedCoordinates)
    onlineProblemData.AddSolution(onlineSolution)
    onlineProblemData.UncompressSolution("T", reducedOrderBases["T"])

    PW.WriteCompressedSolution(mesh, onlineReducedCoordinates, reducedOrderBases["T"], "T_reduced")
    print("The compressed solution has been written in PXDMF Format")

    PW.WriteReducedOrderBasis(mesh, reducedOrderBases["T"], "T")
    print("The reduced order basis has been written in PXDMF Format")


    #CHECK ACCURACY

    solutionFileName = folder + "square.ut"
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)

    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 1

    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()
    solutionT = S.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)
    for t in outputTimeSequence:
        T = solutionReader.ReadSnapshot("TP", t, nbeOfComponentsPrimal, primality=True)
        solutionT.AddSnapshot(T, t)


    print("check T")
    rel = []
    for t in onlineReducedCoordinates.keys():
        exact = solutionT.GetSnapshotAtTime(t)
        normExact = np.linalg.norm(exact)
        reconstructed = np.dot(reducedOrderBases["T"].T, onlineReducedCoordinates[t])
        relError = np.linalg.norm(reconstructed - exact)
        if normExact > 0:
            relError /= normExact
        rel.append(relError)

    print("rel error =", rel)

    ZSW.WriteZsetSolution(mesh, meshFileName, "reduced", collectionProblemData, onlineProblemData, "T")
    ZSW.WriteZsetSolution(mesh, meshFileName, "ROB", collectionProblemData, onlineProblemData, outputReducedOrderBasis = True)


    folderHandler.SwitchToExecutionFolder()

    assert np.max(rel) < 1.e-2, "!!! Regression detected !!! ROMErrors have become too large"



if __name__ == "__main__":
    test()

