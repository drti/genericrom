# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from genericROM.FE import FETools as FT
from genericROM.DataCompressors import IncrementalSnapshotPOD as SP
from genericROM.OperatorCompressors import Thermal_transient as Th
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os



def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    folder = GetTestDataPath()+"Zset"+os.sep+"ThermalSequential"+os.sep

    inputFileName = folder + "square.inp"
    meshFileName = folder + "square.geof"
    solutionFileName = folder + "square.ut"

    meshReader = ZMR.ZsetMeshReader(meshFileName)
    inputReader = ZIR.ZsetInputReader(inputFileName)
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)


    mesh = meshReader.ReadMesh()
    print("Mesh defined in " + meshFileName + " has been read")


    print("ComputeL2ScalarProducMatrix...")
    snapshotCorrelationOperator = {}
    snapshotCorrelationOperator["T"] = FT.ComputeL2ScalarProducMatrix(mesh, 1)
    print("...done")


    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 1

    print("PreCompressOperator...")
    operatorPreCompressionData = Th.PreCompressOperator(mesh, "ALLBOUNDARY")
    print("...done")



    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()

    solutionT = S.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()

    problemData = PD.ProblemData(folder)
    problemData.AddSolution(solutionT)
    problemData.AddConstitutiveLaw(constitutiveLawsList)

    collectionProblemData = CPD.CollectionProblemData()
    collectionProblemData.AddVariabilityAxis("config", str)
    collectionProblemData.DefineQuantity("T", "temperature", "K")
    collectionProblemData.AddProblemData(problemData, config="case-1")

    NUM = 25

    count = 0
    for time in outputTimeSequence:
        T = solutionReader.ReadSnapshot("TP", time, nbeOfComponentsPrimal, primality=True)
        solutionT.AddSnapshot(T, time)

        if count%NUM == NUM-1:


            SP.CompressData(collectionProblemData, "T", 1.e-4, snapshotCorrelationOperator["T"], compressSolutions = True)
            reducedOrderBasisT = collectionProblemData.GetReducedOrderBasis("T")
            print("Size reducedOrderBasisT =", reducedOrderBasisT.shape[0])

            Th.CompressOperator(collectionProblemData, operatorPreCompressionData, mesh, 1.e-3)

            solutionT.RemoveSnapshots([outputTimeSequence[count-i] for i in range(NUM)])

        count += 1



    SIO.SaveState("collectionProblemData", collectionProblemData)
    SIO.SaveState("snapshotCorrelationOperator", snapshotCorrelationOperator)



    for time in outputTimeSequence:
        T = solutionReader.ReadSnapshot("TP", time, nbeOfComponentsPrimal, primality=True)
        solutionT.AddSnapshot(T, time)


    collectionProblemData.CompressSolutions("T", snapshotCorrelationOperator["T"])

    CompressedSolutionT = solutionT.GetReducedCoordinates()
    reducedOrderBasisT = collectionProblemData.GetReducedOrderBasis("T")


    compressionErrors = []

    for t in outputTimeSequence:
        reconstructedCompressedSolution = np.dot(CompressedSolutionT[t], reducedOrderBasisT)
        exactSolution = solutionT.GetSnapshot(t)
        norml2ExactSolution = np.linalg.norm(exactSolution)
        if norml2ExactSolution != 0:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)/norml2ExactSolution
        else:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)
        compressionErrors.append(relError)

    print("compressionErrors =", compressionErrors)

    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-2, "!!! Regression detected !!! compressionErrors have become too large"


if __name__ == "__main__":
    test()

