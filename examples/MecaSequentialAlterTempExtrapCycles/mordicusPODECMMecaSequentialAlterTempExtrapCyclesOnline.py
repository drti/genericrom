# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from genericROM.IO import ZsetSolutionWriter as ZSW
from Mordicus.Containers import Solution as S
from genericROM.OperatorCompressors import Mechanical as Meca
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os
import pytest


@pytest.mark.zmat
def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    ##################################################
    # LOAD DATA FOR ONLINE
    ##################################################

    collectionProblemData = SIO.LoadState("collectionProblemData")

    operatorCompressionData = collectionProblemData.GetOperatorCompressionData("U")

    reducedOrderBases = collectionProblemData.GetReducedOrderBases()


    ##################################################
    # ONLINE
    ##################################################

    folder = GetTestDataPath()+"Zset"+os.sep+"MecaAlternateTempDefinition"+os.sep

    inputFileName = folder + "cube2cycles.inp"
    inputReader = ZIR.ZsetInputReader(inputFileName)

    meshFileName = folder + "cube.geof"
    mesh = ZMR.ReadMesh(meshFileName)

    onlineProblemData = collectionProblemData.GetProblemData(config = "case-1")
    onlineProblemData.SetDataFolder(os.path.relpath(folder, folderHandler.scriptFolder))


    timeSequence = inputReader.ReadInputTimeSequence()

    inputLoadingTemperature = inputReader.ConstructLoadingsList(loadingTags = ["temperature"])[0]
    assert inputLoadingTemperature.type == "temperature"


    onlineProblemDataTempLoading = onlineProblemData.GetLoadingsOfType("temperature")[0]
    onlineProblemDataTempLoading.UpdateLoading(inputLoadingTemperature)
    onlineProblemDataTempLoading.ReduceLoading(mesh)


    import time
    start = time.time()
    onlineCompressedSolution = Meca.ComputeOnline(onlineProblemData, timeSequence, operatorCompressionData, 1.e-8)
    print(">>>> DURATION ONLINE =", time.time() - start)
    onlineData = onlineProblemData.GetOnlineData("U")

    numberOfIntegrationPoints = onlineProblemData.GetSolution("evrcum").numberOfDOFs


    dualNames = ["evrcum", "sig12", "sig23", "sig31", "sig11", "sig22", "sig33", "eto12", "eto23", "eto31", "eto11", "eto22", "eto33"]


    for name in dualNames:
        solutionsDual = S.Solution(name, 1, numberOfIntegrationPoints, primality = False)

        onlineDualCompressedSolution, errorGappy = Meca.ReconstructDualQuantity(name, operatorCompressionData, onlineData, timeSequence = list(onlineCompressedSolution.keys()))

        solutionsDual.SetReducedCoordinates(onlineDualCompressedSolution)

        onlineProblemData.AddSolution(solutionsDual)


    onlineEvrcumCompressedSolution = onlineProblemData.GetSolution("evrcum").GetReducedCoordinates()


    ## Compute Error

    nbeOfComponentsPrimal = onlineProblemData.GetSolution("U").nbeOfComponents
    numberOfNodes = onlineProblemData.GetSolution("U").numberOfNodes


    solutionFileName = GetTestDataPath()+"Zset"+os.sep+"MecaAlternateTempDefinition"+os.sep+"cube.ut"
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)
    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()

    solutionEvrcumExact  = S.Solution("evrcum", 1, numberOfIntegrationPoints, primality = False)
    solutionUExact = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)
    for t in outputTimeSequence:
        evrcum = solutionReader.ReadSnapshotComponent("evrcum", t, primality=False)
        solutionEvrcumExact.AddSnapshot(evrcum, t)
        U = solutionReader.ReadSnapshot("U", t, nbeOfComponentsPrimal, primality=True)
        solutionUExact.AddSnapshot(U, t)

    solutionEvrcumApprox = S.Solution("evrcum", 1, numberOfIntegrationPoints, primality = False)
    solutionEvrcumApprox.SetReducedCoordinates(onlineEvrcumCompressedSolution)
    solutionEvrcumApprox.UncompressSnapshots(reducedOrderBases["evrcum"])

    solutionUApprox = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)
    solutionUApprox.SetReducedCoordinates(onlineCompressedSolution)
    solutionUApprox.UncompressSnapshots(reducedOrderBases["U"])

    ROMErrorsU = []
    ROMErrorsEvrcum = []
    for t in outputTimeSequence:
        exactSolution = solutionEvrcumExact.GetSnapshotAtTime(t)
        approxSolution = solutionEvrcumApprox.GetSnapshotAtTime(t)
        norml2ExactSolution = np.linalg.norm(exactSolution)
        if norml2ExactSolution > 1.e-10:
            relError = np.linalg.norm(approxSolution-exactSolution)/norml2ExactSolution
        else:
            relError = np.linalg.norm(approxSolution-exactSolution)
        ROMErrorsEvrcum.append(relError)

        exactSolution = solutionUExact.GetSnapshotAtTime(t)
        approxSolution = solutionUApprox.GetSnapshotAtTime(t)
        norml2ExactSolution = np.linalg.norm(exactSolution)
        if norml2ExactSolution > 1.e-10:
            relError = np.linalg.norm(approxSolution-exactSolution)/norml2ExactSolution
        else:
            relError = np.linalg.norm(approxSolution-exactSolution)
        ROMErrorsU.append(relError)


    print("ROMErrors U =", ROMErrorsU)
    print("ROMErrors Evrcum =", ROMErrorsEvrcum)


    onlineProblemData.AddSolution(solutionUApprox)


    ZSW.WriteZsetSolution(mesh, meshFileName, "reduced", collectionProblemData, onlineProblemData, "U")


    folderHandler.SwitchToExecutionFolder()

    assert np.max(ROMErrorsU) < 1.e-4, "!!! Regression detected !!! ROMErrors have become too large"
    assert np.max(ROMErrorsEvrcum) < 1.e-3, "!!! Regression detected !!! ROMErrors have become too large"



if __name__ == "__main__":


    from Muscat.Helpers import Profiler as P
    p = P.Profiler()
    p.Start()

    test()

    p.Stop()
    print(p)


