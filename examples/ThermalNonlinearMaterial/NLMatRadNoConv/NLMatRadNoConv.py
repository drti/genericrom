# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import os, sys

from genericROM import GetTestDataPath, GetExamplesPath


def test():

    sys.path.insert(0,GetExamplesPath())

    import templateOffline as TOff
    import templateOnline as TOn

    absoluteAdress = os.path.realpath(__file__)

    folderFile = os.path.dirname(os.path.realpath(__file__))
    parentFolderFile = os.path.basename(os.path.dirname(folderFile))

    folderName = GetTestDataPath()+"Zset"+os.sep+parentFolderFile+os.sep+os.path.basename(folderFile)+os.sep

    TOff.RunTemplate(absoluteAdress, folderName, 1.e-5, radiationSet = "ALLBOUNDARY")
    TOn.RunTemplate(absoluteAdress, folderName, 1.e-3)


if __name__ == "__main__":
    test()
