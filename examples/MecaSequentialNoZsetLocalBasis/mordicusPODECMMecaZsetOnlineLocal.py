# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import Solution as S
from genericROM.IO import XDMFWriter as XW
from genericROM.OperatorCompressors import Mechanical
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os
import time


def test():


    start = time.time()

    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()

    ##################################################
    # LOAD DATA FOR ONLINE
    ##################################################

    collectionProblemDatas = [SIO.LoadState("mordicusState_Basis_0"), SIO.LoadState("mordicusState_Basis_1")]

    operatorCompressionDatas = [collectionProblemDatas[i].GetOperatorCompressionData("U") for i in range(2)]
    reducedOrderBases = [collectionProblemDatas[i].GetReducedOrderBases() for i in range(2)]

    snapshotCorrelationOperator = SIO.LoadState("snapshotCorrelationOperator")


    ##################################################
    # ONLINE
    ##################################################


    folder = GetTestDataPath()+"Zset"+os.sep+"MecaSequentialNoZsetLocalBasis"+os.sep+"Computation1"+os.sep

    inputFileName = folder + "cube.inp"
    inputReader = ZIR.ZsetInputReader(inputFileName)

    meshFileName = folder + "cube.geof"
    mesh = ZMR.ReadMesh(meshFileName)

    onlineProblemData = PD.ProblemData("Online")
    onlineProblemData.SetDataFolder(os.path.relpath(folder, folderHandler.scriptFolder))

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()
    onlineProblemData.AddConstitutiveLaw(constitutiveLawsList)

    loadingList = inputReader.ConstructLoadingsList()
    onlineProblemData.AddLoading(loadingList)


    initialCondition = inputReader.ConstructInitialCondition()
    onlineProblemData.SetInitialCondition(initialCondition)

    initialCondition.ReduceInitialSnapshot(reducedOrderBases[0], snapshotCorrelationOperator)


    timeSequence = inputReader.ReadInputTimeSequence()
    timeSequences = [timeSequence[:len(timeSequence)//2], timeSequence[len(timeSequence)//2-1:]]


    nbeOfComponentsPrimal = 3
    onlinesolution = S.Solution("U", nbeOfComponentsPrimal, mesh.GetNumberOfNodes(), primality = True)
    onlineProblemData.AddSolution(onlinesolution)


    start = time.time()

    onlineReducedCoordinates = []

    for i in range(2):

        for loading in onlineProblemData.GetLoadingsForSolution("U"):
            loading.ReduceLoading(mesh, onlineProblemData, reducedOrderBases[i], operatorCompressionDatas[i])

        onlineCompressedSolution = Mechanical.ComputeOnline(onlineProblemData, timeSequences[i], operatorCompressionDatas[i], 1.e-4)

        onlineReducedCoordinates.append(onlineCompressedSolution)

        for t, reducedCoordinates in onlineReducedCoordinates[i].items():
            onlinesolution.AddReducedCoordinates(reducedCoordinates, t)

        if i==0:
            previousTime = timeSequences[i][-1]

            projectedReducedOrderBasis = collectionProblemDatas[0].GetDataCompressionData("U")["projectedReducedOrderBasis_1"]
            onlinesolution.ConvertReducedCoordinatesReducedOrderBasisAtTime(projectedReducedOrderBasis, previousTime)
            onlineProblemData.GetInitialCondition().SetReducedInitialSnapshot("U", onlinesolution.GetReducedCoordinatesAtTime(previousTime))



    print("duration online =", time.time() - start)

    #CHECK ACCURACY

    solutionFileName = folder + "cube.ut"
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)

    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 3

    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()
    solution = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)
    for t in outputTimeSequence:
        U = solutionReader.ReadSnapshot("U", t, nbeOfComponentsPrimal, primality=True)
        solution.AddSnapshot(U, t)

    solutionUApprox = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)

    print("check U")
    rel = []
    for t in onlinesolution.GetTimeSequenceFromReducedCoordinates():
        if t < timeSequence[len(timeSequence)//2-1]:
            i = 0
        else:
            i = 1
        exact = solution.GetSnapshotAtTime(t)
        normExact = np.linalg.norm(exact)
        reconstructed = np.dot(reducedOrderBases[i]["U"].T, onlineReducedCoordinates[i][t])
        solutionUApprox.AddSnapshot(reconstructed, t)

        relError = np.linalg.norm(reconstructed - exact)
        if normExact > 0:
            relError /= normExact
        rel.append(relError)

    print("rel error =", rel)

    XW.WriteSolution(mesh, solutionUApprox, 'OnlineU')
    print("The compressed solution has been written in XDMF Format")

    folderHandler.SwitchToExecutionFolder()

    assert np.max(rel) < 0.1, "!!! Regression detected !!! ROMErrors have become too large"


if __name__ == "__main__":

    from Muscat.Helpers import Profiler as P
    p = P.Profiler()
    p.Start()

    test()

    p.Stop()
    print(p)