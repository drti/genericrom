# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from genericROM.FE import FETools as FT
from genericROM.DataCompressors import FusedSnapshotPOD as SP
from genericROM.OperatorCompressors import Mechanical
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os


def test():

    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    meshFileName = "cube.geof"
    solutionFileName = "cube.ut"

    folder0 = GetTestDataPath()+"Zset"+os.sep+"MecaSequentialNoZsetLocalBasis"+os.sep

    meshReader = ZMR.ZsetMeshReader(folder0+os.sep+"Computation1"+os.sep+meshFileName)

    print("Reading Mesh...")
    mesh = meshReader.ReadMesh()
    print("...done")

    print("ComputeL2ScalarProducMatrix...")
    snapshotCorrelationOperator = {}
    snapshotCorrelationOperator["U"] = FT.ComputeL2ScalarProducMatrix(mesh, 3)

    SIO.SaveState("snapshotCorrelationOperator", snapshotCorrelationOperator)
    print("...done")

    numberOfNodes = mesh.GetNumberOfNodes()
    numberOfIntegrationPoints = FT.ComputeNumberOfIntegrationPoints(mesh)
    nbeOfComponentsPrimal = 3
    nbeOfComponentsDual = 6

    print("PreCompressOperator...")
    operatorPreCompressionData = Mechanical.PreCompressOperator(mesh)
    print("...done")

    collectionProblemDatas = []

    for i in range(2):

        collectionProblemData = CPD.CollectionProblemData()

        collectionProblemData.AddVariabilityAxis('config',
                                                str,
                                                description="dummy variability")
        collectionProblemData.DefineQuantity("U", "displacement", "m")
        collectionProblemData.DefineQuantity("sigma", "stress", "Pa")


        collectionProblemDatas.append(collectionProblemData)

        folders = [folder0+"Computation1"+os.sep, folder0+"Computation2"+os.sep]

        for j, folder in enumerate(folders):

            #inputReader = ZIR.ZsetInputReader(folder+inputFileName)
            solutionReader = ZSR.ZsetSolutionReader(folder+solutionFileName)

            solutionU = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)
            solutionSigma = S.Solution("sigma", nbeOfComponentsDual, numberOfIntegrationPoints, primality = False)
            outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()
            if i==0:
                outputTimeSequence = outputTimeSequence[:len(outputTimeSequence)//2+2]
            elif i==1:
                outputTimeSequence = outputTimeSequence[len(outputTimeSequence)//2-2:]


            #constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()

            problemData = PD.ProblemData("case-"+str(i)+"_"+str(j))
            problemData.AddSolution(solutionU)
            problemData.AddSolution(solutionSigma)
            #problemData.AddConstitutiveLaw(constitutiveLawsList)

            collectionProblemData.AddProblemData(problemData, config="case-"+str(i)+"_"+str(j))

            for time in outputTimeSequence:
                U = solutionReader.ReadSnapshot("U", time, nbeOfComponentsPrimal, primality=True)
                solutionU.AddSnapshot(U, time)
                sigma = solutionReader.ReadSnapshot("sig", time, nbeOfComponentsDual, primality=False)
                solutionSigma.AddSnapshot(sigma, time)

        SP.CompressData(collectionProblemData, "U", 1.e-4, snapshotCorrelationOperator["U"] )

        Mechanical.CompressOperator(collectionProblemData, operatorPreCompressionData, mesh, 1.e-3)

        print("check compression...")
        reducedOrderBasis = collectionProblemData.GetReducedOrderBasis("U")
        collectionProblemData.CompressSolutions("U", snapshotCorrelationOperator["U"])

        compressionErrors = []
        for j, folder in enumerate(folders):
            problemData = collectionProblemData.GetProblemData(config="case-"+str(i)+"_"+str(j))
            solutionU = problemData.GetSolution("U")
            outputTimeSequence = solutionU.GetTimeSequenceFromSnapshots()
            reducedCoordinatesU = solutionU.GetReducedCoordinates()
            solutionUApprox = S.Solution("U", nbeOfComponentsPrimal, numberOfNodes, primality = True)
            solutionUApprox.SetReducedCoordinates(reducedCoordinatesU)
            solutionUApprox.UncompressSnapshots(reducedOrderBasis)
            for t in outputTimeSequence:
                approxSolution = solutionUApprox.GetSnapshotAtTime(t)
                exactSolution = solutionU.GetSnapshotAtTime(t)
                norml2ExactSolution = np.linalg.norm(exactSolution)
                if norml2ExactSolution != 0:
                    relError = np.linalg.norm(approxSolution-exactSolution)/norml2ExactSolution
                else:
                    relError = np.linalg.norm(approxSolution-exactSolution)
                compressionErrors.append(relError)
        print("compressionErrors =", compressionErrors)

    for i in range(2):

        dataCompressionData = {}
        for j in [j for j in range(2) if j != i]:
            reducedOrderBasisJ = collectionProblemDatas[j].GetReducedOrderBasis("U")
            dataCompressionData["projectedReducedOrderBasis_"+str(j)] = collectionProblemDatas[i].ComputeReducedOrderBasisProjection("U", reducedOrderBasisJ, snapshotCorrelationOperator["U"])

        collectionProblemDatas[i].AddDataCompressionData("U", dataCompressionData)

        SIO.SaveState("mordicusState_Basis_"+str(i), collectionProblemDatas[i])


    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-5, "!!! Regression detected !!! compressionErrors have become too large"


if __name__ == "__main__":

    from Muscat.Helpers import Profiler as P
    p = P.Profiler()
    p.Start()

    test()

    p.Stop()
    print(p)