# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import Solution as S
from genericROM.IO import XDMFWriter as XW
from genericROM.OperatorCompressors import Thermal_transient as Th
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os



def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    ##################################################
    # LOAD DATA FOR ONLINE
    ##################################################

    collectionProblemDatas = [SIO.LoadState("mordicusState_Basis_0"), SIO.LoadState("mordicusState_Basis_1")]

    operatorCompressionDatas = [collectionProblemDatas[i].GetOperatorCompressionData("T") for i in range(2)]
    reducedOrderBases = [collectionProblemDatas[i].GetReducedOrderBases() for i in range(2)]

    snapshotCorrelationOperator = SIO.LoadState("snapshotCorrelationOperator")


    ##################################################
    # ONLINE
    ##################################################


    folder = GetTestDataPath()+"Zset"+os.sep+"ThermalLocalBasis"+os.sep+"Computation1"+os.sep

    inputFileName = folder + "square.inp"
    inputReader = ZIR.ZsetInputReader(inputFileName)

    meshFileName = folder + "square.geof"
    mesh = ZMR.ReadMesh(meshFileName)

    onlineProblemData = PD.ProblemData("Online")
    onlineProblemData.SetDataFolder(os.path.relpath(folder, folderHandler.scriptFolder))

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()
    onlineProblemData.AddConstitutiveLaw(constitutiveLawsList)

    loadingList = inputReader.ConstructLoadingsList()
    onlineProblemData.AddLoading(loadingList)

    initialCondition = inputReader.ConstructInitialCondition()
    onlineProblemData.SetInitialCondition(initialCondition)

    initialCondition.ReduceInitialSnapshot(reducedOrderBases[0], snapshotCorrelationOperator)


    timeSequence = inputReader.ReadInputTimeSequence()
    timeSequences = [timeSequence[:len(timeSequence)//2], timeSequence[len(timeSequence)//2-1:]]


    onlineSolutionT = S.Solution("T", 1, mesh.GetNumberOfNodes(), primality = True)
    onlineProblemData.AddSolution(onlineSolutionT)


    onlineReducedCoordinates = []

    for i in range(2):

        for loading in loadingList:
            loading.ReduceLoading(mesh, onlineProblemData, reducedOrderBases[i], operatorCompressionDatas[i])

        onlineReducedCoordinates.append(Th.ComputeOnline(onlineProblemData, timeSequences[i], operatorCompressionDatas[i], 1.e-4))

        for t, reducedCoordinates in onlineReducedCoordinates[i].items():
            onlineSolutionT.AddReducedCoordinates(reducedCoordinates, t)

        if i==0:
            previousTime = timeSequences[i][-1]

            projectedReducedOrderBasis = collectionProblemDatas[0].GetDataCompressionData("projectedReducedOrderBasis_1")
            onlineSolutionT.ConvertReducedCoordinatesReducedOrderBasisAtTime(projectedReducedOrderBasis, previousTime)
            onlineProblemData.GetInitialCondition().SetReducedInitialSnapshot("T", onlineSolutionT.GetReducedCoordinatesAtTime(previousTime))


    #CHECK ACCURACY

    solutionFileName = folder + "square.ut"
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)

    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 1

    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()
    solutionT = S.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)
    for t in outputTimeSequence:
        T = solutionReader.ReadSnapshot("TP", t, nbeOfComponentsPrimal, primality=True)
        solutionT.AddSnapshot(T, t)

    solutionTApprox = S.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)

    print("check T")
    rel = []
    for t in outputTimeSequence:
        if t < timeSequence[len(timeSequence)//2-1]:
            i = 0
        else:
            i = 1
        exact = solutionT.GetSnapshotAtTime(t)
        normExact = np.linalg.norm(exact)
        reconstructed = np.dot(reducedOrderBases[i]["T"].T, onlineReducedCoordinates[i][t])
        solutionTApprox.AddSnapshot(reconstructed, t)

        relError = np.linalg.norm(reconstructed - exact)
        if normExact > 0:
            relError /= normExact
        rel.append(relError)

    print("rel error =", rel)

    XW.WriteSolution(mesh, solutionTApprox, 'OnlineT')
    print("The compressed solution has been written in XDMF Format")

    folderHandler.SwitchToExecutionFolder()

    assert np.max(rel) < 1.e-2, "!!! Regression detected !!! ROMErrors have become too large"


if __name__ == "__main__":
    test()
