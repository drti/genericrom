# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.IO import ZsetSolutionReader as ZSR
from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from genericROM.FE import FETools as FT
from genericROM.DataCompressors import FusedSnapshotPOD as SP
#from genericROM.DataCompressors import IncrementalSnapshotPOD as SP
from genericROM.OperatorCompressors import Thermal_transient as Th
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
from genericROM import GetTestDataPath
import numpy as np
import os


def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()


    inputFileName = "square.inp"
    meshFileName = "square.geof"
    solutionFileName = "square.ut"

    folder0 = GetTestDataPath()+"Zset"+os.sep+"ThermalLocalBasis"+os.sep

    meshReader = ZMR.ZsetMeshReader(folder0+os.sep+"Computation1"+os.sep+meshFileName)

    print("Reading Mesh...")
    mesh = meshReader.ReadMesh()
    print("...done")

    print("ComputeL2ScalarProducMatrix...")
    snapshotCorrelationOperator = {}
    snapshotCorrelationOperator["T"] = FT.ComputeL2ScalarProducMatrix(mesh, 1)
    SIO.SaveState("snapshotCorrelationOperator", snapshotCorrelationOperator)
    print("...done")

    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 1

    print("PreCompressOperator...")
    operatorPreCompressionData = Th.PreCompressOperator(mesh, "ALLBOUNDARY")
    print("...done")

    collectionProblemDatas = []

    for i in range(2):

      collectionProblemData = CPD.CollectionProblemData()
      collectionProblemData.AddVariabilityAxis("config", str)
      collectionProblemData.DefineQuantity("T", "temperature", "K")
      collectionProblemDatas.append(collectionProblemData)

      folders = [folder0+"Computation1"+os.sep, folder0+"Computation2"+os.sep]

      for j, folder in enumerate(folders):

        inputReader = ZIR.ZsetInputReader(folder+inputFileName)
        solutionReader = ZSR.ZsetSolutionReader(folder+solutionFileName)

        solutionT = S.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)
        outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()
        if i==0:
            outputTimeSequence = outputTimeSequence[:len(outputTimeSequence)//2+2]
        elif i==1:
            outputTimeSequence = outputTimeSequence[len(outputTimeSequence)//2-2:]

        constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()

        problemData = PD.ProblemData("case-"+str(i)+"_"+str(j))
        problemData.AddSolution(solutionT)
        problemData.AddConstitutiveLaw(constitutiveLawsList)

        collectionProblemData.AddProblemData(problemData, config="case-"+str(i)+"_"+str(j))

        for t in outputTimeSequence:
            T = solutionReader.ReadSnapshot("TP", t, nbeOfComponentsPrimal, primality=True)
            solutionT.AddSnapshot(T, t)

        SP.CompressData(collectionProblemData, "T", 1.e-4, snapshotCorrelationOperator["T"], compressSolutions = True)

        Th.CompressOperator(collectionProblemData, operatorPreCompressionData, mesh, 1.e-3)

        print("check compression...")
        reducedOrderBasisT = collectionProblemData.GetReducedOrderBasis("T")
        collectionProblemData.CompressSolutions("T", snapshotCorrelationOperator["T"])
        CompressedSolutionT = solutionT.GetReducedCoordinates()
        compressionErrors = []
        for t in outputTimeSequence:
            reconstructedCompressedSolution = np.dot(CompressedSolutionT[t], reducedOrderBasisT)
            exactSolution = solutionT.GetSnapshot(t)
            norml2ExactSolution = np.linalg.norm(exactSolution)
            if norml2ExactSolution != 0:
                relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)/norml2ExactSolution
            else:
                relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)
            compressionErrors.append(relError)
        print("compressionErrors =", compressionErrors)


    for i in range(2):
        for j in [j for j in range(2) if j != i]:
            reducedOrderBasisJ = collectionProblemDatas[j].GetReducedOrderBasis("T")
            projectedReducedOrderBasis = collectionProblemDatas[i].ComputeReducedOrderBasisProjection("T", reducedOrderBasisJ, snapshotCorrelationOperator["T"])
            collectionProblemDatas[i].AddDataCompressionData("projectedReducedOrderBasis_"+str(j), projectedReducedOrderBasis)

        SIO.SaveState("mordicusState_Basis_"+str(i), collectionProblemDatas[i])


    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-4, "!!! Regression detected !!! compressionErrors have become too large"


if __name__ == "__main__":
    test()
