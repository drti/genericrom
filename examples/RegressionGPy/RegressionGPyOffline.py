# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from genericROM.DataCompressors import FusedSnapshotPOD as FSP
from genericROM.OperatorCompressors import Regression
from genericROM.Containers.OperatorCompressionData.Regressors import Gpy as G
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
import numpy as np
import pytest

@pytest.mark.skip(reason="GPy dependency bugged")
def test():

    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()

    sol = SIO.LoadState("sol")

    solutionName = "TP"
    nbeOfComponents = 1
    numberOfNodes = 676
    primality = True

    collectionProblemData = CPD.CollectionProblemData()

    collectionProblemData.AddVariabilityAxis('Text', float, quantities=('temperature', 'K'), description="this is Text")
    collectionProblemData.AddVariabilityAxis('Tint', float, quantities=('temperature', 'K'), description="this is Tint")

    collectionProblemData.DefineQuantity(solutionName, "Temperature", "Celsius")

    parameters = [[100.0, 1000.0], [90.0, 1100.0], [110.0, 900.0], [105.0, 1050.0]]

    for i in range(4):

        problemData = PD.ProblemData("myProblem_"+str(i))

        outputTimeSequence = []

        solution = S.Solution(
            solutionName=solutionName,
            nbeOfComponents=nbeOfComponents,
            numberOfNodes=numberOfNodes,
            primality=primality,
        )

        problemData.AddSolution(solution)

        for time, snapshot in sol[i].items():
            solution.AddSnapshot(snapshot, time)
            problemData.AddParameter(np.array(parameters[i] + [time]), time)
            outputTimeSequence.append(time)

        collectionProblemData.AddProblemData(problemData, Text=parameters[i][0], Tint=parameters[i][1])


    print("Solutions have been read")

    ##################################################
    # OFFLINE
    ##################################################

    FSP.CompressData(collectionProblemData, solutionName, 1.e-2, compressSolutions = True)
    print("A reduced order basis has been computed has been constructed using SnapshotPOD")

    regressors = [G.Gpy("TP", options = {"kernel":"Matern52", "optim":"bfgs", "max_iters":1000, "num_restarts":1})]

    Regression.CompressOperator(collectionProblemData, regressors)

    SIO.SaveState("collectionProblemData", collectionProblemData)


    ##################################################
    ## check accuracy compression
    ##################################################

    reducedOrderBasis = collectionProblemData.GetReducedOrderBasis("TP")

    compressionErrors = []
    for _, problemData in collectionProblemData.GetProblemDatas().items():
        solution = problemData.GetSolution("TP")
        CompressedSolution = solution.GetReducedCoordinates()
        for t in solution.GetTimeSequenceFromReducedCoordinates():
            reconstructedReducedCoordinates = np.dot(CompressedSolution[t], reducedOrderBasis)
            exactSolution = solution.GetSnapshotAtTime(t)
            norml2ExactSolution = np.linalg.norm(exactSolution)
            if norml2ExactSolution != 0:
                relError = np.linalg.norm(reconstructedReducedCoordinates-exactSolution)/norml2ExactSolution
            else:
                relError = np.linalg.norm(reconstructedReducedCoordinates-exactSolution)
            compressionErrors.append(relError)

    print("compressionErrors =", compressionErrors)

    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-2, "!!! Regression detected !!! compressionErrors have become too large"



if __name__ == "__main__":
    test()
