# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.Containers import ProblemData as PD
from genericROM.OperatorCompressors import Regression
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
import numpy as np
import pytest


@pytest.mark.skip(reason="GPy dependency bugged")
def test():

    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()

    ##################################################
    # LOAD DATA FOR ONLINE
    ##################################################

    collectionProblemData = SIO.LoadState("collectionProblemData")
    reducedOrderBasis = collectionProblemData.GetReducedOrderBasis("TP")
    onlineCompressionData = collectionProblemData.GetOperatorCompressionData("TP")

    ##################################################
    # ONLINE
    ##################################################

    onlineProblemData = PD.ProblemData("Online")

    OnlineTimeSequence = np.array(np.arange(200, 1001, 200), dtype=float)

    for t in OnlineTimeSequence:
        onlineProblemData.AddParameter(np.array([95.0, 950.0] + [t]), t)

    onlineProblemData.AddOnlineData(onlineCompressionData)

    reducedCoordinates = Regression.ComputeOnline(onlineProblemData, "TP")


    ##################################################
    #check accuracy of prediction
    ##################################################

    ref = SIO.LoadState("ref")

    compressionErrors = []
    for t in reducedCoordinates.keys():
        reconstructedCompressedSolution = np.dot(reducedCoordinates[t], reducedOrderBasis)
        exactSolution = ref[t]
        norml2ExactSolution = np.linalg.norm(exactSolution)
        if norml2ExactSolution != 0:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)/norml2ExactSolution
        else:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)
        compressionErrors.append(relError)

    folderHandler.SwitchToExecutionFolder()

    print("compressionErrors =", compressionErrors)

    assert np.max(compressionErrors) < 0.01, "!!! Regression detected !!! compressionErrors have become too large"



if __name__ == "__main__":
    test()
