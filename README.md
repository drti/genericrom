Safran Reduced Order Modeling library "genericROM".


# 1) INSTALLATION


The dependencies of genericROM are:

* for users:

  * python>=3.9 (tested 3.9 with mfront/mgis, 3.11 without)
  * [matplotlib](https://matplotlib.org/)
  * [Mordicus](https://gitlab.com/mor_dicus/mordicus)
  * [Muscat](https://gitlab.com/drti/muscat)
  * Optional: [MGIS](https://thelfer.github.io/mgis/web/index.html), [Mfront](http://tfel.sourceforge.net/)
  * Optional: [Z-mat](http://www.zset-software.com/products/z-mat/)

* for developers, running tests and building the documentation, add the developers dependencies of Mordicus, namely:

  * setuptools>=61.0.0
  * pip
  * pytest
  * pytest-cov
  * sphinx
  * sphinx-rtd-theme

Users can install the conda package with

    conda install -c drti genericrom

Alternatively, and for developers, the repository can be downloaded with

    git clone https://gitlab.com/drti/genericrom.git

Then, once the required dependencies have been installed, path/to/genericrom/src should be added to the PYTHONPATH environment variable.
To use the Z-mat optional dependency, one should compile a python wrapper using

    cd path/to/genericROM
    python setup.py compile

A conda package can be also created using the recipe available in the repository in

    path/to/genericrom/recipes/linux-64/



#### Remark on optional dependencies


The solid mechanics ROMs with nonlinear constitutive laws require the use of a constitutive law solver.
Two optionnal constitutive law solvers can be used:

* [Zmat](http://www.zset-software.com): (environment variable: set Z7PATH and append $Z7PATH/calcul to PATH, append $Z7PATH/PUBLIC/lib-Linux_64/mkl:$Z7PATH/PUBLIC/lib-Linux_64 to LD_LIBRARY_PATH)
* [MFront](http://tfel.sourceforge.net/install.html) , with [MGIS](https://thelfer.github.io/mgis/web/index.html)

The user is responsible for the installation of these solvers and the acception of the terms of use defined in their respective licenses.


#### Remark for mpi4py

Warning on clusters: the use of mpi4py may require an install from pip, after loading a mpi installation adapted to the cluster.
You may need to install the Mordicus from scratch if you want to use a custom version of mpi (and be mindful of which mpirun you are using).



# 2) TESTING

There are two different type of tests, both using pytest and both checking non-regression
by compararing the produced values with precomputed references.

## Unit testing

Checks all functions and produces a coverage report.

    cd path/to/genericROM
    pytest tests --cov=src/genericROM --cov-report=html:path/to/bluid/html

Depending on the installation of optional Zmat and MFront constitutive law solvers, tests can be deactivated with the "zmat" and "mfront" pytests markers. For instance, to run the sequential tests without Zmat nor MFront:

    cd path/to/genericROM
    pytest -m "not zmat and not mfront" tests

## Simplified use cases

Illustrates the functionalities of the library in complete use cases.

    cd path/to/genericROM
    pytest -m "not mpi" examples
    mpirun -n 2 pytest -m "mpi" examples

Optional examples requiring Zmat and MFront can be deactivated in the same fashion as the previous section.

# 3) DOCUMENTATION


A sphinx documentation can be generated from the main folder:

    cd path/to/genericROM
    python setup.py build_sphinx

The documentation is also available on [readthedocs](https://genericROM.readthedocs.io/).

# 4) CONTRIBUTION AND LICENSE

See the CONTRIBUTING.md and LICENSE files.
