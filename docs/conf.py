#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import os, sys
import configparser


sys.path.insert(0,os.path.abspath('../src/genericROM'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
]

templates_path = ['_templates']
autosummary_generate = False
source_suffix = '.rst'
master_doc = 'index'

cfg = configparser.ConfigParser()
cfg.read('../setup.cfg')

project = cfg['metadata']['name']
copyright = cfg['metadata']['copyright']
copyright_holder = cfg['metadata']['copyright_holder']
license = cfg['metadata']['license']
version = cfg['metadata']['version']
release = cfg['metadata']['version']
author  = cfg['metadata']['copyright']

language = 'en'
numfig = True

pygments_style = 'sphinx'
todo_include_todos = False

html_theme = 'sphinx_rtd_theme'
html_title = 'genericROM'

html_static_path = ['_static']
html_css_files = ['custom.css']
html_logo = "_static/logo.png"
html_theme_options = {
    'logo_only': False,
    'navigation_depth': 3,
}

htmlhelp_basename = 'genericROMDoc'
add_module_names = False



examplesMecaFolders = [
    'MecaSequential',
    'MecaParallel',
    'MecaParallelBiMat',
    'MecaSequentialGappyMetaModel',
    'MecaSequentialGappyBiaisMetaModel',
    'MecaSequentialGappyBiaisMetaModel2',
    'MecaSequentialLinear',
    'MecaSequentialNoZsetLocalBasis',
]
examplesThermalFolders = [
    'ThermalSequential',
    'ThermalParallel',
    'ThermalLocalBasis',
]


examplesThermalMaterialFolders = [
    'ThermalLinearMaterial/LinMatNoRadConv',
    'ThermalLinearMaterial/LinMatRadConv',
    'ThermalLinearMaterial/LinMatRadNoConv',
    'ThermalNonlinearMaterial/NLMatNoRadConv',
    'ThermalNonlinearMaterial/NLMatRadConv',
    'ThermalNonlinearMaterial/NLMatRadNoConv',
]


def make_zipfile(output_filename, source_dir):
    import shutil
    shutil.make_archive(output_filename, 'zip', source_dir)



def run_apidoc(_):
    import sphinx.ext.apidoc
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    target_dir = os.path.join(cur_dir, '_source')
    module = os.path.join(cur_dir, '../src/genericROM')
    template_dir = os.path.join(cur_dir, templates_path[0])

    make_zipfile(os.path.join(cur_dir, '_tutorials/TestsData'), os.path.join(cur_dir, '../tests/TestsData'))

    for folder in examplesMecaFolders:
        make_zipfile(os.path.join(cur_dir, '_tutorials/Mechanical/'+folder+'/example'+folder), os.path.join(cur_dir, '../examples/'+folder))
    for folder in examplesThermalFolders:
        make_zipfile(os.path.join(cur_dir, '_tutorials/Thermal/'+folder+'/example'+folder), os.path.join(cur_dir, '../examples/'+folder))

    import zipfile
    for folder in examplesThermalMaterialFolders:
        splittedFolder = folder.split('/')
        make_zipfile(os.path.join(cur_dir, '_tutorials/Thermal/'+folder+'/example'+splittedFolder[1]), os.path.join(cur_dir, '../examples/'+folder))
        zip = zipfile.ZipFile(os.path.join(cur_dir, '_tutorials/Thermal/'+folder+'/example'+splittedFolder[1])+'.zip','a')
        zip.write(os.path.join(cur_dir, '../examples/templateOffline.py'), 'templateOffline.py')
        zip.write(os.path.join(cur_dir, '../examples/templateOnline.py'),  'templateOnline.py')
        zip.close()

    sphinx.ext.apidoc.main(['-T', '-M', '-e', '-f', '-t', template_dir, '-o', target_dir, module])


def setup(app):
    app.add_css_file('custom.css')
    app.connect('builder-inited', run_apidoc)



