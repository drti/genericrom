==================
genericROM library
==================


The open-source library 'genericROM' is developed at Safran.
The code is hosted on Gitlab_.

.. _Gitlab: https://gitlab.com/drti/genericROM

The library uses the datamodel developed in the Mordicus_ code by the FUI MOR_DICUS consortium,
as well as the Muscat_ library, developed at Safran.

.. _Mordicus: https://gitlab.com/mor_dicus/mordicus
.. _Muscat: https://gitlab.com/drti/muscat

It consists of reduced order modeling modules with the following features:

* **physics**: the reduced model solves the same equations physics as the reference high-fidelity model (physical reduced order modeling: physical ROM).
* **genericity**: the method is not dedicated to a particular physical model. At the moment, nonlinear mechanical and nonlinear transient thermal problems are handled.
* **nonintrusivity**: the library can reduce snapshots computed by commercial software, and do not require to modify internal assembly routine of the reference high-fidelity code
* **scalability**: parallel computed with distributed memory can be used to handle large problems


The genericROM library has been used in some publications, see :ref:`Publications`.


.. toctree::
   :maxdepth: 2
   :caption: Getting started

   intro

.. toctree::
   :maxdepth: 2
   :caption: Methodology

   _methods/index

.. toctree::
   :maxdepth: 3
   :caption: Tutorials

   _tutorials/index


.. toctree::
   :maxdepth: 2
   :caption: Autodoc

   _source/genericROM

.. toctree::
   :caption: Appendix

   genindex

