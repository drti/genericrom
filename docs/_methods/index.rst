.. _Methodology:

=================
Numerical methods
=================

The library relies on the data model and the workflow proposed in Mordicus, see
the `Mordicus methodology documentation`_ for details.

The main feature of genericROM is the nonlinear projection-based model-order reduction,
using snapshot Proper Orthogonal Decomposition (snapshot-POD) for the data compression,
and Empirical Cubature Method (ECM) for the operator compression steps.
We briefly illustrate the main concepts here for nonlinear structural mechanics.
More details being available in the Section :ref:`Publications` (article 1
for nonlinear structural mechanics; article 2 for nonlinear transient thermal problems).


.. _HFMsec:

High-Fidelity Model (HFM)
=========================


Consider a structure denoted :math:`\Omega`, whose boundary :math:`\partial\Omega` is partitioned as
:math:`\partial\Omega=\partial\Omega_D\cup\partial\Omega_N` such that
:math:`\partial\Omega_D^{\mathrm{o}}\cap\partial\Omega_N^{\mathrm{o}}=\emptyset`, see :numref:`image-domain`.

.. figure:: _images/domain.jpg
  :name: image-domain
  :align: center
  :width: 25%

  Schematic representation of the structure of interest.


The structure is subjected to a quasi-static time-dependent loading, composed of an homogeneous Dirichlet boundary conditions on
:math:`\partial\Omega_D` and Neumann boundary conditions on
:math:`\partial\Omega_N` in the form of a prescribed traction :math:`T_N`, as well as a volumic force :math:`f`.
The setting depends on some variability :math:`\mu`, which can be a parameter vector, or represent some nonparametrized
variability.
The evolution of the displacement :math:`u_\mu(x,t)` over :math:`(x,t)\in\Omega\times[0,T]` is governed by equations:

.. math::

  \begin{alignat}{3}
  &\epsilon(u_\mu)=\frac{1}{2}\left(\nabla u_\mu+ (\nabla u_\mu)^T\right)&\rm{~in~}\Omega\times[0,T]&\quad\rm{(compatibility equation),}\\
  &{\rm div}\left(\sigma_\mu\right)+f_\mu=0&\rm{~in~}\Omega\times[0,T]&\quad\rm{(equilibrium equation),}\\
  &\sigma_\mu=\sigma(\epsilon(u_\mu),y_\mu)&\rm{~in~}\Omega\times[0,T]&\quad\rm{(constitutive~law),}\\
  &u_\mu=0&\rm{~in~}\partial\Omega_D\times[0,T]&\quad\rm{(prescribed~zero~displacement),}\\
  &\sigma_\mu\cdot n_{\partial\Omega}=T_{\mu,N}&\rm{~in~}\partial\Omega_N\times[0,T]&\quad\rm{(prescribed~traction),}\\
  &u_\mu=0,y_\mu=0&\rm{~in~}\Omega~{\rm at~t=0}&\quad\rm{(initial~condition),}
  \end{alignat}

where :math:`\epsilon` is the linear strain tensor, :math:`\sigma_\mu` is the Cauchy stress
tensor, :math:`y_\mu` denotes the internal variables of the constitutive law
and :math:`n_{\partial\Omega}` is the outward normal vector on :math:`\partial\Omega`.
We precise that the evolution of the internal variables :math:`y_\mu` is updated while solving the constitutive law.

Define :math:`H^1_0(\Omega)=\{v\in L^2(\Omega)|~\frac{\partial v}{\partial x_i}\in L^2(\Omega),~1\leq i\leq 3{\rm~and~}v|_{\partial\Omega_D}=0\}`.
Denote :math:`\{\varphi_i\}_{1\leq i\leq N}\in\mathbb{R}^{N\times N}`, a finite element basis whose span, denoted :math:`\mathcal{V}`,
constitutes an approximation of :math:`H^1_0(\Omega)^3`; :math:`N` is the number of finite element basis functions,
hence the number of degrees of freedom of the discretized prediction.
A discretized weak formulation reads:
find :math:`u_\mu\in \mathcal{V}` such that for all :math:`v\in \mathcal{V}`,

.. math::

  \int_{\Omega}\sigma(\epsilon(u_\mu),y):\epsilon(v)=\int_{\Omega}f_\mu\cdot v+\int_{\partial\Omega_N}T_{\mu,N}\cdot v,

that we denote for concision :math:`\mathcal{F}_\mu\left(u_\mu\right)=0`. A Newton algorithm can be used to solve this
nonlinear global equilibrium problem at each time step:

.. math::

  \displaystyle\frac{D\mathcal{F}_\mu}{Du}\left(u_\mu^k\right)\left(\boldsymbol {u}_\mu^{k+1}-\boldsymbol {u}_\mu^{k}\right)=-\mathcal{F}_\mu\left(u_\mu^k\right),

where

.. math::

  \displaystyle{\frac{D\mathcal{F}_\mu}{Du}\left({u}^k\right)}_{ij}=\int_{\Omega}\epsilon\left(\varphi_j\right):\mathcal{K}\left(\epsilon(u_\mu^k),y_\mu\right):\epsilon\left(\varphi_i\right),

and

.. math::

  \displaystyle {\mathcal{F}_\mu\left({u}_\mu^k\right)}_i=\int_\Omega\sigma\left(\epsilon({u}_\mu^k),y_\mu\right):\epsilon\left(\varphi_i\right)-\int_\Omega f_\mu\cdot\varphi_i-\int_{\partial\Omega_N}T_{\mu,N}\cdot\varphi_i.

In the two relations above, :math:`\mathcal{K}\left(\epsilon(u_\mu^k),y_\mu\right)` is the local tangent operator,
:math:`u_\mu^k\in\mathcal{V}` is the k-th iteration of the discretized displacement field at the current time-step, and
:math:`\boldsymbol {u}_\mu^k=\left(u_{\mu,i}^k\right)_{1\leq i\leq N}\in\mathbb{R}^N` is such that
:math:`\displaystyle u_\mu^k=\sum_{i=1}^Nu_{\mu,i}^k\varphi_i`.
In particular, :math:`f_\mu`, :math:`T_{\mu,N}`, :math:`u_\mu^k` and :math:`y_\mu` are known and enforce the time-dependence
of the solution. Depending on the constitutive law, the computation of the functions
:math:`\displaystyle\left(u_\mu^k,y\right)\mapsto \sigma\left(\epsilon(u_\mu^k),y_\mu\right)` and
:math:`\displaystyle\left(u_\mu^k,y\right)\mapsto \mathcal{K}\left(\epsilon(u_\mu^k),y_\mu\right)` can involve complex
differential-algebraic system of equations.


.. _ProjROM:

Projection-based Reduced-Order Modeling (ROM)
=============================================

The object of this section is to provide synthetic elements on the numerical methods,
and some implementation choices made in the library genericROM.

As explained in `Mordicus methodology documentation`_, usual ROM workflows contain an **offline** (learning) phase and
an **online** (exploitation) phase.



We start by explaining the **online** phase and presenting the ROM. Since we want to construct and solve it in the
most efficient way, the **offline** phase is dedicated to precompute as many steps as possible, under the considered variability.



.. _Online:

Online phase
------------

The reduced-order model is obtained by assembling the physical problem in the same fashion as the HFM in Section :ref:`HFMsec`,
with the difference that the finite element basis :math:`(\varphi_i)_{1\leq i\leq N}\in\mathbb{R}^{N\times N}`, is replaced by
a reduced-order basis (ROB) :math:`(\psi_i)_{1\leq i\leq n}\in\mathbb{R}^{n\times N}`, with :math:`n\ll N`.
Hence, the reduced Newton algorithm is constructed as

.. math::

  \displaystyle\frac{D\mathcal{F}_\mu}{D\hat{u}}\left(\hat{u}_\mu^k\right)\left(\hat{\boldsymbol{u}}_\mu^{k+1}-\hat{\boldsymbol{u}}_\mu^{k}\right)=-\mathcal{F}_\mu\left(\hat{u}_\mu^k\right),

where

.. math::

  \displaystyle{\frac{D\mathcal{F}_\mu}{D\hat{u}}\left(\hat{u}_\mu^k\right)}_{ij}=\int_{\Omega}\epsilon\left(\psi_j\right):\mathcal{K}\left(\epsilon(\hat{u}_\mu^k),y_\mu\right):\epsilon\left(\psi_i\right)

and

.. math::

  \displaystyle {\mathcal{F}_\mu\left(\hat{u}_\mu^k\right)}_i=\int_\Omega\sigma\left(\epsilon(\hat{u}_\mu^k),y\right):\epsilon\left(\psi_i\right)-\int_\Omega f_\mu\cdot\psi_i-\int_{\partial\Omega_N}T_{\mu,N}\cdot\psi_i.

In the two relations above, :math:`\hat{u}_\mu^k\in\hat{\mathcal{V}}:={\rm Span}\left(\psi_i\right)_{1\leq i\leq n}`
is the :math:`k`-th iteration of the reduced displacement field for the current time-step and
:math:`\hat{\boldsymbol{u}}_\mu^k=\left(\hat{u}^k_{\mu, i}\right)_{1\leq i\leq n}\in\mathbb{R}^n` is such
:math:`\displaystyle\hat{u}_\mu^k=\sum_{i=1}^n\hat{u}^k_{\mu, i}\psi_i`.

The **online** stage is said efficient if the reduced problem can be constructed and solved
in computational complexity independent of :math:`N`. When the variability :math:`\mu` is parametrized, efficiency
is possible by precomputing various terms. With nonparametrized variability, depending on its nature, some assembling
task with a linear complexity in :math:`N` may be required at the beginning of the **online** stage
(for instance for a boundary condition). All these scenarios are handled by genericROM.

The **offline** phase contains three steps, for which we present below the methodological choices made in genericROM,
as well as intuition and implementation details.

.. _Offline:

Offline phase
-------------

Data generation
~~~~~~~~~~~~~~~

This step corresponds to the generation of the snapshots by solving the high-fidelity model.
In genericROM, projection-based ROMs can be constructed even when the snapshots are generated by commercial software,
as long as a reader for the computed solution is available (or can be developed).

Data compression
~~~~~~~~~~~~~~~~

This step corresponds to the generation of the ROB :math:`(\psi_i)_{1\leq i\leq n}`.
The simplest method available in genericROM is the snapshot-POD:

1. Choose a tolerance :math:`\epsilon_{\rm POD}`.
2. Compute the correlation matrix :math:`C_{i,j}=\int_{\Omega}u_i\cdot u_j`, :math:`1\leq i,j\leq N_s`, where :math:`N_s` is the total number of HFM snapshots.
3. Compute the :math:`\epsilon_{\rm POD}`-truncated eigendecomposition of :math:`C`: :math:`\xi_i\in\mathbb{R}^{N_c}` and :math:`\lambda_i>0`, where :math:`1\leq i\leq n`, are the :math:`n` first eigenvector and eigenvalues.
4. Compute the reduced order basis :math:`\displaystyle\psi_i(x)=\frac{1}{\sqrt{\lambda_i}}\sum_{j=1}^{N_s}u_j(x){\xi_i}_j`, :math:`1\leq i\leq n`.


The snapshot-POD is also available in parallel with distributed memory, by partitioning the domain :math:`\Omega` in subdomains.


Operator compression
~~~~~~~~~~~~~~~~~~~~

This step corresponds to the additional treatment needed to guarantee the efficiency of the **online** stage,
by pre-processing of the computationally demanding integration over :math:`\Omega` and :math:`\partial\Omega_N`.
Notice that for computing the reduced Newton algorithm in Section :ref:`Online`,
without additional approximation, the numerical integration step will
in practice strongly limit the efficiency of the ROM (no interesting speedup with respect to the computation of
the high-fidelity model will be obtained in practice).
In genericROM, an implementation of the Empirical Cubature Method (ECM) is available to tackle this problem.
A concise description is given below, more details are available in Section :ref:`Publications`, article 1.


In the finite element method, the integrals in the HFM are computed using high-fidelity
quadrature formulas, which, when applied to the reduced internal forces vector, read

.. math::

  \hat{F}^{\rm int}_i(t):=\int_\Omega\sigma\left(\epsilon(\hat{u}),y\right)(x,t):\epsilon\left(\psi_i\right)(x)=\sum_{e\in E}\sum_{k=1}^{n_e}\omega_k\sigma\left(\epsilon(\hat{u}),y\right)(x_k,t):\epsilon\left(\psi_i\right)(x_{k}), 1\leq i\leq n,

where :math:`E` denotes the set of elements of the mesh, :math:`n_e` the number of integration points for
the element :math:`e`, :math:`\omega_k` and :math:`x_k` are the integration weights and points of the considered
element. We define :math:`N_G` the total number of integration points.

The ECM consists in replacing the high-fidelity quadratures by a reduced quadrature with positive weights,
which, when applied to the reduced internal forces vector, read

.. math::

  \hat{F}^{\rm int}_i(t)\approx\sum_{k'=1}^{n_g}\hat{\omega}_{k'}\sigma\left(\epsilon(\hat{u}),y\right)(\hat{x}_{k'},t):\epsilon\left(\psi_i\right)(\hat{x}_{k'}), 1\leq i\leq n,

where :math:`\hat{\omega}_{k'}>0` and :math:`\hat{x}_{k'}` are respectively the reduced integration weights and
points, and :math:`n_g\ll N_G` is the length of the reduced quadrature.

Computation of the reduced quadrature:

Denote :math:`f_q:=\sigma\left(\epsilon(u_{(q//n)+1}),y\right):\epsilon\left(\psi_{(q\%n)+1}\right)`, :math:`1\leq q\leq nN_c`.
where :math:`//`  and :math:`\%` denote respectively the quotient and the remainder of the Euclidean division.
Denote also :math:`\mathcal{Z}^{n_G}` a subset of :math:`[1;N_G]` of size :math:`n_G` and :math:`J_{\mathcal{Z}^{n_G}}\in\mathbb{R}^{nN_c\times n_G}`
and :math:`\boldsymbol{b}\in\mathbb{N}^{nN_c}` such that for all :math:`1\leq q\leq nN_c` and all :math:`1\leq q'\leq n_G`,

.. math::

  J_{\mathcal{Z}^{n_G}} = \Bigg(f_q(x_{\mathcal{Z}^{n_G}_{q'}})\Bigg)_{1\leq q\leq nN_c,~q'\in\mathcal{Z}^{n_G}},\qquad \boldsymbol{b} = \left(\int_{\Omega}f_q\right)_{1\leq q\leq nN_c},

where :math:`\mathcal{Z}^{n_G}_{q'}` denotes the :math:`q'`-th element of :math:`\mathcal{Z}^{n_G}`. Recall that :math:`n` is
the number of snapshot POD modes. Let :math:`\hat{\boldsymbol{\omega}}\in{\mathbb{R}^{+}}^n_G`,
:math:`\displaystyle\left(J_{\mathcal{Z}^{n_G}}\hat{\boldsymbol{\omega}}\right)_q=\sum_{q'=1}^{n_G}\hat{\omega}_{q'}\sigma\left(\epsilon(u_{{(q//n)+1}}),y\right)(x_{\mathcal{Z}^{n_G}_{q'}}):\epsilon\left(\psi_{(q\%n)+1}\right)(x_{\mathcal{Z}_{q'}})`,
:math:`1\leq q\leq nN_c` is a candidate approximation for
:math:`\displaystyle \int_{\Omega}\sigma\left(\epsilon(u_{{(q//n)+1}}),y\right):\epsilon\left(\psi_{(q\%n)+1}\right) = b_q`~,
:math:`1\leq q\leq nN_c`. The problem of finding the best reduced quadrature formula of length :math:`n_G` for
the reduced internal forces vector can be written:

.. math::

  \left(\hat{\omega},\mathcal{Z}^{n_G}\right)=\arg\underset{\hat{\omega}'\in{\mathbb{R}^{+}}^{n_G},\mathcal{Z}'^{n_G}\subset[1;N_G]}{\min}\left\|J_{\mathcal{Z}'^{n_G}}\hat{\omega}'-b\right\|,

where :math:`\left\|\cdot\right\|` stands for the Euclidean norm. Taking the length of the reduced quadrature
formula in the objective function of the optimization leads to a NP-hard problem, which in approximately
solved in genericROM using an Nonnegative Orthogonal Matching Pursuit algorithm.


.. _Implementation_details:

======================
Implementation details
======================


Noninstrusivity
===============

GenericROM enables the handling of projection-based ROM workflow in a nonintrusive manner: the user do not need
to modify assembling routine of the reference code used to solve the HFM. For instance, ROM can be constructed
using data generated by commercial code like Ansys and Abaqus can be used (see :ref:`Publications`, article 2,
for the construction of a ROM for nonlinear transient thermal problem using data generated by Abaqus).
To do so, the assembling routine of operators and right-hand side are handling directly in genericROM,
using the finite element engine of Muscat_ developped at Safran.

.. _Muscat: https://gitlab.com/drti/muscat



Precomputations for efficiency
==============================

In ROM workflows, the common measure of efficiency is the speedup, defined as the ratio between the computation duration
of the HFM and the one of the ROM. For a given accuracy, the higher the speedup, the better.
Various elements can be taken into account, including code optimization of the **online** stage.

The methods enabling algorithmic complexity gains have been presented in Section :ref:`Offline`.
In this section, we give additional implementation elements and illustrations to give some intuition on the
mechanisms at play.

Depending on the considered equations and nature of variability, the goal is to precompute as many quantities
as possible in the **offline** stage, to leave as few operations as possible to the **online** stage.

In the HFM, the assembling of the global tangent operator at each Newton iteration, using the
high-fidelity quadrature, reads:

.. math::

  \displaystyle{\frac{D\mathcal{F}_\mu}{Du}\left({u}^k\right)}_{ij}=\sum_{g=1}^{N_g}\omega_g\left[\epsilon\left(\varphi_j\right):\mathcal{K}\left(\epsilon(u_\mu^k),y_\mu\right):\epsilon\left(\varphi_i\right)\right]\left(x_g\right),~1\leq i,j\leq N.

In the proposed ROM, the assembling of the reduced global tangent operator at each Newton iteration, using the
reduced quadrature, reads:

.. math::

  \displaystyle{\frac{D\mathcal{F}_\mu}{D\hat{u}}\left(\hat{u}^k\right)}_{ij}\approx\sum_{g=1}^{n_g}\hat{\omega}_g\left[\epsilon\left(\psi_j\right):\mathcal{K}\left(\epsilon(\hat{u}_\mu^k),y_\mu\right):\epsilon\left(\psi_i\right)\right]\left(\hat{x}_g\right),~1\leq i,j\leq n\ll N.


The corresponding linear systems are illustrated on :numref:`HFMmatrix` and :numref:`ROMmatrix`.
In particular, :math:`n` should be much smaller than :math:`N` to obtain a significant speedup
despite the fact that the linear system is sparse in the HFM and dense in the ROM.

.. figure:: _images/HFMmatrix.jpg
  :name: HFMmatrix
  :align: center
  :width: 50%

  HFM: a large sparse linear system is assembled and solved at each step of the Newton algorithm.


.. figure:: _images/ROMmatrix.jpg
  :name: ROMmatrix
  :align: center
  :width: 35%

  ROM: a small dense linear system is assembled and solved at each step of the reduced Newton algorithm.


Based on :numref:`HFMmatrix` and :numref:`ROMmatrix`, the speedup is clear for the resolution
part of the linear system. We still need to illustrate how precomputations in the **offline** phase
allow an efficient assembling of the reduced problem.
Denote :math:`d` the number of unknowns of second-order tensor dual quantities.
In 3D, :math:`d=6`, for instance for the strain tensor,
the unknowns are :math:`\epsilon_{11}, \epsilon_{22}, \epsilon_{33}, \epsilon_{12}, \epsilon_{23}, \epsilon_{31}`.
The reduced global tangent operator at each Newton iteration, using the reduced quadrature, can be written

.. math::

  \displaystyle{\frac{D\mathcal{F}_\mu}{D\hat{u}}\left(\hat{u}^k\right)}_{ij}\approx\sum_{g=1}^{n_g}\hat{\omega}_g\sum_{l=1}^d\left[\epsilon_l\left(\psi_j\right)\left(\hat{x}_g\right)\right]\sum_{m=1}^d\left[\mathcal{K}_{l,m}\left(\epsilon(\hat{u}_\mu^k),y_\mu\right)\left(\hat{x}_g\right)\right]\left[\epsilon_m\left(\psi_i\right)\left(\hat{x}_g\right)\right],~1\leq i,j


In genericROM, the code for computing this quantity in the **online** stage is

.. code:: python

  reducedTangentMatrix = np.einsum('g,lgj,glm,mgi->ij', reducedIntegrationWeights, reducedEpsilonAtReducedIntegPoints,
                                   localTgtMat, reducedEpsilonAtReducedIntegPoints, optimize = True)

where

* ``reducedIntegrationWeights[g]`` = :math:`\omega_g`,
* ``reducedEpsilonAtReducedIntegPoints[lgj]`` = :math:`\epsilon_l\left(\psi_j\right)\left(\hat{x}_g\right)`,
* ``localTgtMat[glm]`` = :math:`\mathcal{K}_{l,m}\left(\epsilon(\hat{u}_\mu^k),y_\mu\right)\left(\hat{x}_g\right)`.

The object ``reducedEpsilonAtReducedIntegPoints`` is a third-order tensor containing the components of the strain tensor applied to the ROB and
evaluated at the reduced integration points. This quantity is precomputed in the **offline** stage.
The object ``localTgtMat`` is a third-order tensor containing the components of the local tangent matrix evaluated at the reduced integration points:
this quantity is computed online by the constitutive law solver.


.. _Publications:

============
Publications
============


The numerical illustrations in the following articles have been computed using genericROM (or a previous version
of the library). In particular, the methology for the mechanical and thermal reduced order modeling are detailed
respectively in the first two articles:

.. list-table:: Publications using genericROM (or a previous version of the library)
   :widths: 5 70 25
   :header-rows: 1
   :class: tight-table

   * - Number
     - Informations
     - Illustration
   * - 1
     - F. Casenave, N. Akkari, F. Bordeu, C. Rey and D. Ryckelynck, `A nonintrusive distributed reduced order modeling framework for nonlinear structural mechanics – application to elastoviscoplastic computations`_, Int. J. Numer. Meth. Eng. 121 (2020) 32–53
     - .. figure:: _images/IJNME.jpg
         :align: center
         :width: 100%
   * - 2
     - F. Casenave, A. Gariah, C. Rey and F. Feyel, `A nonintrusive reduced order model for nonlinear transient thermal problems with nonparametrized variability`_, Adv. Model. Simul. Eng. Sci., 7(22) (2020)
     - .. figure:: _images/AMSES.jpg
         :align: center
         :width: 100%
   * - 3
     - F. Casenave and N. Akkari, `An Error Indicator-Based Adaptive Reduced Order Model for Nonlinear Structural Mechanics—Application to High-Pressure Turbine Blades`_, Math. Comput. Appl. 24, 41 (2019)
     - .. figure:: _images/MCA.jpg
         :align: center
         :width: 100%
   * - 4
     - F. Casenave, N. Akkari and D. Ryckelynck, `Reduced Order Modeling assisted by Convolutional Neural Network for thermal problems with nonparametrized geometrical variability`_, Intelligent Computing. SAI 2020. Advances in Intelligent Systems and Computing, vol 1229. Springer, Cham (2020)
     - .. figure:: _images/Computing.jpg
         :align: center
         :width: 100%
   * - 5
     - T. Daniel, F. Casenave, N. Akkari and D. Ryckelynck, `Model order reduction assisted by deep neural networks (ROM-net)`_, Adv. Model. Simul. Eng. Sci., 7(16), (2020)
     - .. figure:: _images/AMSES2.jpg
         :align: center
         :width: 100%
   * - 6
     - T. Daniel, F. Casenave, N. Akkari and D. Ryckelynck, `Data augmentation and feature selection for automatic model recommendation in computational physics`_, Math. Comput. Appl., 26(1), 17 (2021)
     - .. figure:: _images/MCA2.jpg
         :align: center
         :width: 100%
   * - 7
     - T. Daniel, F. Casenave, N. Akkari, A. Ketata and D. Ryckelynck, `Physics-informed cluster analysis and a priori efficiency criterion for the construction of local reduced-order bases`_, J. Comput. Phys., 458, 111120 (2022)
     - .. figure:: _images/JCP.jpg
         :align: center
         :width: 100%
   * - 8
     - T. Daniel, F. Casenave, N. Akkari, D. Ryckelynck and C. Rey, `Uncertainty quantification for industrial design using dictionaries of reduced order models`_, Mech. Ind., 23(3), 20 (2022)
     - .. figure:: _images/MI.jpg
         :align: center
         :width: 100%


.. _Mordicus methodology documentation: https://mordicus.readthedocs.io/en/latest/_methods/index.html

.. _A nonintrusive distributed reduced order modeling framework for nonlinear structural mechanics – application to elastoviscoplastic computations: https://onlinelibrary.wiley.com/doi/abs/10.1002/nme.6187
.. _A nonintrusive reduced order model for nonlinear transient thermal problems with nonparametrized variability: https://amses-journal.springeropen.com/articles/10.1186/s40323-020-00156-3
.. _An Error Indicator-Based Adaptive Reduced Order Model for Nonlinear Structural Mechanics—Application to High-Pressure Turbine Blades: https://www.mdpi.com/2297-8747/24/2/41
.. _Reduced Order Modeling assisted by Convolutional Neural Network for thermal problems with nonparametrized geometrical variability: https://link.springer.com/chapter/10.1007/978-3-030-52246-9_17
.. _Model order reduction assisted by deep neural networks (ROM-net): https://amses-journal.springeropen.com/track/pdf/10.1186/s40323-020-00153-6
.. _Data augmentation and feature selection for automatic model recommendation in computational physics: https://www.mdpi.com/2297-8747/26/1/17
.. _Physics-informed cluster analysis and a priori efficiency criterion for the construction of local reduced-order bases: https://www.sciencedirect.com/science/article/pii/S0021999122001826
.. _Uncertainty quantification for industrial design using dictionaries of reduced order models: https://www.mechanics-industry.org/articles/meca/full_html/2022/01/mi210093/mi210093.html


..
  * F. Casenave, N. Akkari, F. Bordeu, C. Rey and D. Ryckelynck, `A nonintrusive distributed reduced order modeling framework for nonlinear structural mechanics – application to elastoviscoplastic computations`_, Int. J. Numer. Meth. Eng. 121 (2020) 32–53
  * F. Casenave, A. Gariah, C. Rey and F. Feyel, `A nonintrusive reduced order model for nonlinear transient thermal problems with nonparametrized variability`_, Adv. Model. Simul. Eng. Sci., 7(22) (2020)
  * F. Casenave and N. Akkari, `An Error Indicator-Based Adaptive Reduced Order Model for Nonlinear Structural Mechanics—Application to High-Pressure Turbine Blades`_, Math. Comput. Appl. 24, 41 (2019)
  * F. Casenave, N. Akkari and D. Ryckelynck, `Reduced Order Modeling assisted by Convolutional Neural Network for thermal problems with nonparametrized geometrical variability_`, Intelligent Computing. SAI 2020. Advances in Intelligent Systems and Computing, vol 1229. Springer, Cham (2020)
  * T. Daniel, F. Casenave, N. Akkari and D. Ryckelynck, `Model order reduction assisted by deep neural networks (ROM-net)`_, Adv. Model. Simul. Eng. Sci., 7(16), (2020)
  * T. Daniel, F. Casenave, N. Akkari and D. Ryckelynck, `Data augmentation and feature selection for automatic model recommendation in computational physics`_, Math. Comput. Appl., 26(1), 17 (2021)
  * T. Daniel, F. Casenave, N. Akkari, A. Ketata and D. Ryckelynck, `Physics-informed cluster analysis and a priori efficiency criterion for the construction of local reduced-order bases`_, J. Comput. Phys., 458, 111120 (2022)
  * T. Daniel, F. Casenave, N. Akkari, D. Ryckelynck and C. Rey, `Uncertainty quantification for industrial design using dictionaries of reduced order models`_, Mech. Ind., 23(3), 20 (2022)

