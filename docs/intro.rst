Installation
============

The dependencies of genericROM are:

* for users:

  * python>=3.9 (tested 3.9 with mfront/mgis, 3.11 without)
  * matplotlib_
  * Mordicus_
  * Muscat_
  * Optional: MGIS_, MFront_
  * Optional: Z-mat_

.. _matplotlib: https://matplotlib.org/
.. _Mordicus: https://gitlab.com/mor_dicus/mordicus
.. _Muscat: https://gitlab.com/drti/muscat
.. _Z-mat: http://www.zset-software.com/products/z-mat/
.. _MFront: http://tfel.sourceforge.net/install.html
.. _MGIS: https://thelfer.github.io/mgis/web/index.html

* for developers, running tests and building the documentation, add the developers dependencies of Mordicus, namely:

  * setuptools>=61.0.0
  * pip
  * pytest
  * pytest-cov
  * sphinx
  * sphinx-rtd-theme

Users can install the conda package with

.. code-block:: bash

    conda install -c drti genericrom

MGIS and Mfront optional dependencies can be installed using the following packages

Alternatively, and for developers, the repository can be downloaded with

.. code-block:: bash

    git clone https://gitlab.com/drti/genericrom.git

Then, once the required dependencies have been installed, path/to/genericrom/src should be added to the PYTHONPATH environment variable.
To use the Z-mat optional dependency, one should compile a python wrapper using

.. code-block:: bash

    cd path/to/genericROM
    python setup.py compile

A conda package can be also created using the recipe available in the repository in

.. code-block:: bash

    path/to/genericrom/recipes/linux-64/


Testing
=======

There are two different type of tests, both using pytest and both checking non-regression
by compararing the produced values with precomputed references.


Unit testing
------------

Checks all functions and produces a coverage report.

.. code-block:: bash

    cd path/to/genericROM
    pytest tests --cov=src/genericROM --cov-report=html:path/to/bluid/html

Depending on the installation of optional Zmat and MFront constitutive law solvers, tests can be deactivated with the "zmat" and "mfront" pytests markers. For instance, to run the sequential tests without Zmat nor MFront:

.. code-block:: bash

    cd path/to/genericROM
    pytest -m "not zmat and not mfront" tests

Simplified use cases
--------------------

Illustrates the functionalities of the library in complete use cases, sequential and parallel.


.. code-block:: bash

    cd path/to/genericROM
    pytest -m "not mpi" examples
    mpirun -n 2 pytest -m "mpi" examples



Doc generation
==============

The present sphinx web documentation can be generated from the main folder.

.. code-block:: bash

    cd path/to/genericROM
    python setup.py build_sphinx


License and contribution
========================

* `BSD 3-Clause license <https://gitlab.com/drti/genericROM/-/blob/master/LICENSE>`_
* `Contribution guide <https://gitlab.com/drti/genericROM/-/blob/master/CONTRIBUTING.md>`_
