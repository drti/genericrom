MecaParallelBiMat
=================

This use case aims to validate the methodology POD + ECM for quasistatic elastoviscoplastic computations with Z-set, with a reconstruction of
the dual quantities using the gappy POD, see Section :ref:`Publications`, article 1.

Optional prerequisite: Zmat.

The data needed to execute the tutorials below can be loaded here: :download:`exampleMecaParallelBiMat.zip <exampleMecaParallelBiMat.zip>`



Features: parallel with two materials
-------------------------------------

The physical setting, workflow and reduction strategy are the same as in the tutorial :ref:`MecaParallel` ,
hence we only point out the differences here.

Consider the cube illustrated in Figure  :numref:`cube-bimat`. This cube is splitted in two subdomains,
in order to produce a parallel workflow.


.. figure:: cube_bimat.jpg
    :name: cube-bimat
    :align: center
    :width: 75%

    (left) Dual mesh of the test case with the accumulated plasticity filed at the end of the simulation, (right) mesh and maximal temperature loading field.


In the same fashion as in the tutorial :ref:`MecaParallel`, we specify the number of threads per mpi instance, 2 in
this example.

.. code-block:: python

    from Muscat.Helpers import CPU as C
    C.SetNumberOfThreadsPerInstance(2)

Results
-------

In :numref:`cube-bimat-res`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.


.. figure:: res.jpg
    :name: cube-bimat-res
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the accumulated plasticity ``p`` (left) HFM, (right) pointwise difference between the ROM and the HFM.
    The mesh is exagerately deformed following the displacement field, and cut along the boundary between the two subdomains.

