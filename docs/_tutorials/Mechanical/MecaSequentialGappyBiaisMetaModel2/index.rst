.. _MecaSequentialGappyBiaisMetaModel2:

MecaSequentialGappyBiaisMetaModel2
==================================

This use case aims to validate the methodology POD + ECM for quasistatic elastoviscoplastic computations with Z-set, with a reconstruction of
the dual quantities using a meta model, see Section :ref:`Publications`, articles 1 and 8.

Optional prerequisite: Zmat.

The data needed to execute the tutorials below can be loaded here: :download:`exampleMecaSequentialGappyBiaisMetaModel2.zip <exampleMecaSequentialGappyBiaisMetaModel2.zip>`



Features: replace the gappy-POD with a custom meta-model and correct the bias
-----------------------------------------------------------------------------

The setting is very similar to tutorial :ref:`MecaSequentialGappyBiaisMetaModel`.
We simply explain here how to specify a particular meta-model for the dual
quantity reconstructor, instead of using the default one.
This applies to methods without learning the errors between the HFM and the ROM,
see tutorial :ref:`MecaSequentialGappyMetaModel`.


.. code-block:: python

    from Mordicus.BasicAlgorithms import ScikitLearnRegressor as SLR
    from sklearn.gaussian_process.kernels import WhiteKernel, ConstantKernel, Matern

    kernel = Matern(length_scale=1., nu=2.5) + ConstantKernel(constant_value=1.0, constant_value_bounds=(1.e-12, 1.e12)) * \
        WhiteKernel(noise_level=1, noise_level_bounds=(1e-12, 1e12))
    regressor = SLR.MyGPR(kernel=kernel)
    paramGrid = {'kernel__k1__length_scale': [1., 2.], 'kernel__k1__nu':[1.5, 2.5], 'kernel__k2__k1__constant_value':[1.], \
        'kernel__k2__k2__noise_level':[1]}

    dualReconstructionData = Mechanical.LearnDualReconstruction(collectionProblemData, \
        dualNames, reducedIntegrationPoints, methodDualReconstruction = "MetaModel", \
        timeSequenceForDualReconstruction = timeSequence, \
        snapshotsAtReducedIntegrationPoints = onlineDualQuantityAtReducedIntegrationPoints, \
        regressor = regressor, paramGrid = paramGrid)


The last two attributes of ``dualReconstructionData`` enable to specify any scikit-learn meta-model.

Results
-------

In :numref:`cube-gappybiasmeta2-res`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.

.. figure:: res.jpg
    :name: cube-gappybiasmeta2-res
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the accumulated plasticity ``p`` (left) HFM, (right) pointwise difference between the ROM and the HFM.

We notice that with a more involved meta-model, the accuracy is much better
than tutorials :ref:`MecaSequentialGappyMetaModel` and :ref:`MecaSequentialGappyBiaisMetaModel`.
