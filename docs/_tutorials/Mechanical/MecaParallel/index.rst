.. _MecaParallel:

MecaParallel
============

This use case aims to validate the methodology POD + ECM for quasistatic elastoviscoplastic computations with Z-set, with a reconstruction of
the dual quantities using the gappy POD, see Section :ref:`Publications`, article 1.

Optional prerequisite: Zmat.

The data needed to execute the tutorials below can be loaded here: :download:`exampleMecaParallel.zip <exampleMecaParallel.zip>`



Features: parallel with distributed memory
------------------------------------------

The physical setting, workflow and reduction strategy are the same as in the tutorial :ref:`MecaSequential`,
hence we only point out the differences here.


Consider the cube illustrated in :numref:`cube-parallel`. This cube is splitted in two subdomains,
in order to produce a parallel workflow.


.. figure:: cube_parallel.jpg
    :name: cube-parallel
    :align: center
    :width: 35%

    Mesh splitted in two subdomains.


When using parallel features, one can control how many mpi instances are run, and well as the number of threads per
mpi instance. The number of mpi instance must be taken equal to the number of subdomains, 2 in this example.
The number of threads by per mpi instance, 1 in this example, is enforced as follows:

.. code-block:: python

    from Muscat.Helpers import CPU as C
    C.SetNumberOfThreadsPerInstance(1)


These lines must be written before importing numpy, and other multi-threaded libraries. When nothing is specified,
the number of threads is automatically chosen, which can be problematic in certain cases. For instance, if a large
model is computed on 2 computer nodes, having each a 24-core CPUs, one may want to split the mesh in 48 subdomains,
and setting 1 thread per mpi instance. Not specifying any number of thread may lead to each of the 24 mpi instances
on each node being run on 24 threads.


Results
-------

In :numref:`cube-parallel-res`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.

.. figure:: res.jpg
    :name: cube-parallel-res
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the accumulated plasticity ``p`` (left) HFM, (right) pointwise difference between the ROM and the HFM.
    The mesh is exagerately deformed following the displacement field, and cut along the boundary between the two subdomains.


In :numref:`cube-parallel-rob`, the first two POD modes of the accumulated plasticity ``p`` are illustrated.

.. figure:: ROB.jpg
    :name: cube-parallel-rob
    :align: center
    :width: 75%

    POD modes of the accumulated plasticity ``p``: (left) first, (right) second.
