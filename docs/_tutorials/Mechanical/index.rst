====================
Mechanical tutorials
====================

In addition to the tutorials below, many examples can be found in the path/to/genericROM/examples/Meca* folders.

.. toctree::
   :maxdepth: 2

   MecaSequential/index
   MecaSequentialLinear/index
   MecaParallel/index
   MecaParallelBiMat/index
   MecaSequentialGappyMetaModel/index
   MecaSequentialGappyBiaisMetaModel/index
   MecaSequentialGappyBiaisMetaModel2/index
   MecaSequentialNoZsetLocalBasis/index