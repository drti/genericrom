.. _MecaSequentialGappyMetaModel:

MecaSequentialGappyMetaModel
============================

This use case aims to validate the methodology POD + ECM for quasistatic elastoviscoplastic computations with Z-set, with a reconstruction of
the dual quantities using a meta model, see Section :ref:`Publications`, articles 1 and 8.

Optional prerequisite: Zmat.


The data needed to execute the tutorials below can be loaded here: :download:`exampleMecaSequentialGappyMetaModel.zip <exampleMecaSequentialGappyMetaModel.zip>`



Features: replace the gappy-POD with the default meta-model
-----------------------------------------------------------



The physical setting, workflow and reduction strategy are almost the same as in the tutorial :ref:`MecaSequential` ,
hence we only point out the differences here, namely the use of a meta model to replace the gappy POD in the reconstruction of dual quantities.

Consider the dual quantity accumulated plasticity :math:`p`. The first step is identical to the Gappy-POD, namely computing
a reduced-order basis :math:`p_i` for this quantity. Then, the high-fidelity snapshots of :math:`p` are projected onto
this POD basis to compute the reduced coordinates:

.. math::
    p \approx \sum_{i=1}^{N_p} (p,p_i) p_i = \sum_{i=1}^{N_p} \gamma_i^p p_i.

Then, a meta model, in the form of a scikit-learn regressor, is trained to approximate the following function:

.. math::
    \left(p(\hat{x}_j)\right)_{1\leq j\leq N_{\rm quad}}\mapsto  (\gamma_i^p)_{1\leq i\leq N_p},

where :math:`\hat{x}_j` are the reduced quadrature points selected by the ECM algorithm.

In the online stage, the accumulated plasticity :math:`p` is already computed at the reduced integration points
:math:`\hat{x}_j` to construct the reduced problem, and the trained regressor is used to evaluate the reduced coordinates
of this dual quantity.

.. code-block:: python

    Mechanical.CompressOperator(collectionProblemData, operatorPreCompressionData, \
                                mesh, 1.e-5, listNameDualVarOutput = dualNames, \
                                listNameDualVarGappyIndicesforECM = ["evrcum"],\
                                methodDualReconstruction = "MetaModel",\
                                timeSequenceForDualReconstruction = outputTimeSequence[1:])

When calling the `CompressOperator` function, two attributes are added, with respect to the tutorial :ref:`MecaSequential` :

- ``methodDualReconstruction = "MetaModel"``, by default `scikit-learn's lasso`_  with `alpha`=0.1, `max_iter`=1e8, `tol`=1e-5. Other meta models can be used, see for instance tutoriel :ref:`MecaSequentialGappyBiaisMetaModel2`.
- ``timeSequenceForDualReconstruction``: to specify the time step where the meta model is trained.

.. _scikit-learn's lasso: https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html



Results
-------

In :numref:`cube-gappymeta-res`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.

.. figure:: res.jpg
    :name: cube-gappymeta-res
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the accumulated plasticity ``p`` (left) HFM, (right) pointwise difference between the ROM and the HFM.
