.. _MecaSequentialGappyBiaisMetaModel:

MecaSequentialGappyBiaisMetaModel
=================================

This use case aims to validate the methodology POD + ECM for quasistatic elastoviscoplastic computations with Z-set, with a reconstruction of
the dual quantities using a meta model, see Section :ref:`Publications`, articles 1 and 8.

Optional prerequisite: Zmat.

The data needed to execute the tutorials below can be loaded here: :download:`exampleMecaSequentialGappyBiaisMetaModel.zip <exampleMecaSequentialGappyBiaisMetaModel.zip>`



Features: replace the gappy-POD with the default meta-model and correct the bias
--------------------------------------------------------------------------------


The physical setting, workflow and reduction strategy are almost the same as in the tutorial :ref:`MecaSequential` ,
hence we only point out the differences here, namely the use of a meta model to replace the gappy POD in the reconstruction of dual quantities.
The method is described in the tutorial :ref:`MecaSequentialGappyMetaModel`.

We recall that in the tutorial :ref:`MecaSequentialGappyMetaModel`, the considered
meta-models are trained to approximate the following function:

.. math::
    \left(p(\hat{x}_j)\right)_{1\leq j\leq N_{\rm quad}}\mapsto  (\gamma_i^p)_{1\leq i\leq N_p},

where :math:`\hat{x}_j` are the reduced quadrature points selected by the ECM algorithm,
and then :math:`p(\hat{x}_j)` are values of the HFM snapshots for the dual quantity of
interest :math:`p`.
In this tutorial, we want to account for the potential difference between the
reference high-fidelity model (HFM) and the reduced-order model (ROM) in the training of this meta-model.
Here, a regressor is constructed to approximate the following function:

.. math::
    \left(\hat{p}(\hat{x}_j)\right)_{1\leq j\leq N_{\rm quad}}\mapsto  (\gamma_i^p)_{1\leq i\leq N_p},

where now :math:`\hat{p}(\hat{x}_j)` is the evaluation of the constructed ROM on the
reduced integration points by the constitutive law solver.

We recall that reconstructing the dual-quantities is a post-treatment of the ROM, and
that the reduced-order model can be trained and used without needing to reconstruct the
dual quantities. Hence, the proposed method (explained in :ref:`Publications` , article 8)
consists in first constructing a ROM without training a dual quantity reconstruction algorithm:

.. code-block:: python

    Mechanical.CompressOperator(collectionProblemData, operatorPreCompressionData, \
                                mesh, 1.e-5, listNameDualVarOutput = dualNames, \
                                listNameDualVarGappyIndicesforECM = ["evrcum"],\
                                methodDualReconstruction = None)

In this ``CompressOperator`` configuration, ``methodDualReconstruction`` set to ``None`` enables to
compute the ECM without training a dual quantity reconstructor.

Then, the *online* stage of the ROM is run for the same configurations as the HFM snapshots used
in the *offline* stage. This enables to generate the :math:`\hat{p}(\hat{x}_j)` required to train the dual quantity reconstructor meta-model :

.. code-block:: python

    onlineData = onlineProblemData.GetOnlineData("U")

    timeSequence = np.array(timeSequence)[1:]
    onlineDualQuantityAtReducedIntegrationPoints = {}
    for i, name in enumerate(dualNames):
        onlineDualQuantityAtReducedIntegrationPoints[name] = Mechanical.GetOnlineDualQuantityAtReducedIntegrationPoints(name, onlineData, timeSequence)

    reducedIntegrationPoints = operatorCompressionData.GetReducedIntegrationPoints()

    dualReconstructionData = Mechanical.LearnDualReconstruction(collectionProblemData, dualNames, reducedIntegrationPoints, methodDualReconstruction = "MetaModel", timeSequenceForDualReconstruction = timeSequence, snapshotsAtReducedIntegrationPoints = onlineDualQuantityAtReducedIntegrationPoints)

    operatorCompressionData.SetDualReconstructionData(dualReconstructionData)

The function ``LearnDualReconstruction`` is usually called when using ``CompressOperator``
(except when ``methodDualReconstruction`` is set to ``None``). In this particular
tutorial, we need to call it directly to prevent us from uselessly recomputing the ECM.

The *online* stage is then configured is the same classical fashion as
in the tutorials :ref:`MecaSequential` or :ref:`MecaSequentialGappyMetaModel`.


Results
-------

In :numref:`cube-gappybiasmeta-res`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.

.. figure:: res.jpg
    :name: cube-gappybiasmeta-res
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the accumulated plasticity ``p`` (left) HFM, (right) pointwise difference between the ROM and the HFM.


The gains are not clear with respect to tutorial :ref:`MecaSequentialGappyMetaModel`,
but in the industrial cas considered in :ref:`Publications` , article 8, the gains
are important.