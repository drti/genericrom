
====================
Documented use cases
====================

The data needed to execute the tutorials below can be downloaded here: :download:`TestsData.zip <TestsData.zip>`.
Besides, ``pytest`` is required to run the tutorials (can be installed via conda).
Once decompressed, the following environment variable must be set, for executing the tutorials
presented in this section:

.. code:: bash

   export GENERICROMTESTDATAPATH="/path/to/TestsData"


There is one main tutorial for Mechanical and Thermal problems, respectively :ref:`MecaSequential` and :ref:`ThermalSequential`,
where the complete test case is commented. In the other tutorial, we only comment additional features, without reproducing the complete workflow.


.. toctree::
   :maxdepth: 2
   :caption: Mechanical tutorials

   Mechanical/index


.. toctree::
   :maxdepth: 2
   :caption: Thermal tutorials

   Thermal/index