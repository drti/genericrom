ThermalMaterials
================

In this section, we consider the same use case as in tutorial :ref:`ThermalSequential`,
with various combinations of materials (linear, nonlinear) and of boundary conditions.
As the explanation are identical to tutorial :ref:`ThermalSequential`, we simply provide
the test cases.

No optional prerequisite.


Linear materials
----------------

Without radiation, with convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:download:`exampleLinMatNoRadConv.zip <ThermalLinearMaterial/LinMatNoRadConv/exampleLinMatNoRadConv.zip>`


With radiation, without convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:download:`exampleLinMatRadNoConv.zip <ThermalLinearMaterial/LinMatRadNoConv/exampleLinMatRadNoConv.zip>`

With radiation and convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


:download:`exampleLinMatRadConv.zip <ThermalLinearMaterial/LinMatRadConv/exampleLinMatRadConv.zip>`


Nonlinear materials
-------------------


Without radiation, with convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:download:`exampleNLMatNoRadConv.zip <ThermalNonlinearMaterial/NLMatNoRadConv/exampleNLMatNoRadConv.zip>`

With radiation, without convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:download:`exampleNLMatRadNoConv.zip <ThermalNonlinearMaterial/NLMatRadNoConv/exampleNLMatRadNoConv.zip>`

With radiation and convection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:download:`exampleNLMatRadConv.zip <ThermalNonlinearMaterial/NLMatRadConv/exampleNLMatRadConv.zip>`

