=================
Thermal tutorials
=================

In addition to the tutorials below, many examples can be found in the path/to/genericROM/examples/Thermal* folders.


.. toctree::
   :maxdepth: 2

   ThermalSequential/index
   ThermalParallel/index
   ThermalLocalBasis/index
   thermalMaterialIndex
