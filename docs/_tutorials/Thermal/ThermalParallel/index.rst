.. _ThermalParallel:

ThermalParallel
===============

The data needed to execute the tutorials below can be loaded here: :download:`exampleThermalParallel.zip <exampleThermalParallel.zip>`

This use case aims to validate the methodology POD + ECM for nonlinear transient thermal computations
in parallel with distributed memory, see Section :ref:`Publications`, article 2.

No optional prerequisite: notice that even if the HFM snapshots are computed here
using Z-set, the ROM do not require any Z-set license to be run.


Features: parallel with distributed memory
------------------------------------------


The physical setting, workflow and reduction strategy are the same as in the tutorial :ref:`ThermalSequential`,
hence we only point out the differences here: the parallel setting and the dimensionlality of the test (3D), see :numref:`cube-th-mesh`.
The problem still features a nonlinear material, convection heat flux and radiation boundary conditions.


.. figure:: mesh.jpg
    :name: cube-th-mesh
    :align: center
    :width: 35%

    Mesh splitted in two subdomains.


Results
-------


In :numref:`res_th_par`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.
The comparison is done on the temperature field.


.. figure:: res.jpg
    :name: res_th_par
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the temperatre ``T`` (left) HFM, (right) pointwise difference between the ROM and the HFM.
    The mesh is cut along the boundary between the two subdomains.
