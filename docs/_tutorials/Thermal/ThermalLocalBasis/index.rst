.. _ThermalLocalBasis:

ThermalLocalBasis
=================

The data needed to execute the tutorials below can be loaded here: :download:`exampleThermalLocalBasis.zip <exampleThermalLocalBasis.zip>`

This use case aims to validate the methodology POD + ECM for nonlinear transient thermal computations, see Section :ref:`Publications`, article 2.
This tutorial illustrates in particular how to include data from more than one HFM computation, and how
to construct local reduced-order bases (ROB).

No optional prerequisite: notice that even if the HFM snapshots are computed here
using Z-set, the ROM do not require any Z-set license to be run.


Features: construct 2 local-ROBs from 2 HFM simulations
-------------------------------------------------------


The physical setting is the same as in the tutorial :ref:`ThermalSequential`:
a 2D square mesh, with a nonlinear material, and convection heat flux and radiation bondary conditions.
The data handling and local ROB construction is the same as in the tutorial :ref:`MecaSequentialNoZsetLocalBasis`.


Results
-------

A comparison between the reduced and reference high-fidelity solutions is illustrated in :numref:`res_th_seq_localROB`.

In :numref:`res_th_seq_localROB`, the quality of the reduced model is illustrated by comparing it to the high-fidelity reference.
The comparison is done on the temperature field.


.. figure:: res.jpg
    :name: res_th_seq_localROB
    :align: center
    :width: 75%

    Illustration of the ROM accuracy on the temperatre ``T`` (left) HFM, (right) pointwise difference between the ROM and the HFM.
