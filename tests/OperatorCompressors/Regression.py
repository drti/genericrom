# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import numpy as np
from genericROM.OperatorCompressors import Regression
from Mordicus.Containers import ProblemData
from Mordicus.Containers import CollectionProblemData
from Mordicus.Containers import Solution
from genericROM.DataCompressors import FusedSnapshotPOD as FSP
from genericROM.Containers.OperatorCompressionData.Regressors import Gpy as G
import pytest


@pytest.mark.skip(reason="GPy dependency bugged")
def test():

    numberOfNodes = 20
    nbeOfComponents = 3

    collectionProblemData = CollectionProblemData.CollectionProblemData()
    collectionProblemData.AddVariabilityAxis("config", str)
    collectionProblemData.DefineQuantity("U")

    snapshot = np.ones(nbeOfComponents * numberOfNodes)
    snapshot2 = 2.*np.ones(nbeOfComponents * numberOfNodes)
    parameter = np.array([1.0, 1.0, 0.5, 0.25])
    parameter2 = 2.*np.array([1.0, 1.0, 0.5, 0.25])
    timeSequence = [0., 0.5, 1., 1.5]

    #problemData n1
    problemData = ProblemData.ProblemData("computation1")
    solution = Solution.Solution("U", nbeOfComponents, numberOfNodes, True)
    problemData.AddSolution(solution)
    for t in timeSequence:
        problemData.AddParameter(parameter, t)
        solution.AddSnapshot(snapshot, t)
    collectionProblemData.AddProblemData(problemData, config="case-1")


    #problemData n2
    problemData = ProblemData.ProblemData("computation2")
    solution = Solution.Solution("U", nbeOfComponents, numberOfNodes, True)
    problemData.AddSolution(solution)
    for t in timeSequence:
        problemData.AddParameter(parameter2, t)
        solution.AddSnapshot(snapshot2, t)
    collectionProblemData.AddProblemData(problemData, config="case-2")


    #compute reduced order basis
    FSP.CompressData(collectionProblemData, "U", 1.e-4, compressSolutions = True)

    #compute regression operator
    regressors = [G.Gpy("U", options = {"kernel":"Matern52", "optim":"bfgs", "max_iters":1000, "num_restarts":1})]

    Regression.CompressOperator(collectionProblemData, regressors)

    onlineProblemData = ProblemData.ProblemData("Online")
    onlineProblemData.AddParameter(np.array([0.0, 2.0, 0.5, 0.25]), 0.0)
    onlineProblemData.AddParameter(np.array([2.0, 1.0, 2.0, 0.5]), 3.0)

    operatorCompressionData = collectionProblemData.GetOperatorCompressionData("U")
    onlineProblemData.AddOnlineData(operatorCompressionData)

    reducedCoordinates = Regression.ComputeOnline(onlineProblemData, "U")

    print(reducedCoordinates)

    assert np.abs(reducedCoordinates[0.][0]-11.61895004)/11.61895004 < 1.e-3
    assert np.abs(reducedCoordinates[3.0][0]-11.61895004)/11.61895004 < 1.e-3

    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
