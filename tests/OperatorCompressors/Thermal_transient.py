# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


import numpy as np
import os
from genericROM.OperatorCompressors import Thermal_transient as Th
from Mordicus.Containers import ProblemData
from Mordicus.Containers import CollectionProblemData
from Mordicus.Containers import Solution
from genericROM.DataCompressors import FusedSnapshotPOD as SP
from genericROM.IO import ZsetInputReader as ZIR
from genericROM.IO import ZsetSolutionReader as ZSR
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.FE import FETools as FT
from Mordicus.IO import StateIO as SIO

from genericROM import GetTestDataPath


def test():


    #################################################################
    ### OFFLINE
    #################################################################


    folder = GetTestDataPath() + "Zset"+os.sep+"ThermalSequential"+os.sep

    inputFileName = folder + "square.inp"
    meshFileName = folder + "square.geof"
    solutionFileName = folder + "square.ut"

    meshReader = ZMR.ZsetMeshReader(meshFileName)
    inputReader = ZIR.ZsetInputReader(inputFileName)
    solutionReader = ZSR.ZsetSolutionReader(solutionFileName)


    mesh = meshReader.ReadMesh()
    print("Mesh defined in " + meshFileName + " has been read")



    numberOfNodes = mesh.GetNumberOfNodes()
    nbeOfComponentsPrimal = 1


    outputTimeSequence = solutionReader.ReadTimeSequenceFromSolutionFile()


    solutionT = Solution.Solution("T", nbeOfComponentsPrimal, numberOfNodes, primality = True)

    for time in outputTimeSequence:
        T = solutionReader.ReadSnapshot("TP", time, nbeOfComponentsPrimal, primality=True)
        solutionT.AddSnapshot(T, time)

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()

    problemData = ProblemData.ProblemData(folder)
    problemData.AddSolution(solutionT)
    problemData.AddConstitutiveLaw(constitutiveLawsList)

    collectionProblemData = CollectionProblemData.CollectionProblemData()
    collectionProblemData.AddVariabilityAxis("config", str)
    collectionProblemData.DefineQuantity("T", "temperature", "K")
    collectionProblemData.AddProblemData(problemData, config="case-1")


    print("ComputeL2ScalarProducMatrix...")
    snapshotCorrelationOperator = {}
    snapshotCorrelationOperator["T"] = FT.ComputeL2ScalarProducMatrix(mesh, 1)


    SP.CompressData(collectionProblemData, "T", 1.e-6, snapshotCorrelationOperator["T"])


    print("PreCompressOperator...")
    operatorPreCompressionData = Th.PreCompressOperator(mesh, "ALLBOUNDARY")
    print("...done")

    collectionProblemData.CompressSolutions("T", snapshotCorrelationOperator["T"])


    Th.CompressOperator(collectionProblemData, operatorPreCompressionData, mesh, 1.e-3, toleranceCompressSnapshotsForRedQuad = 1.e-2)
    Th.CompressOperator(collectionProblemData, operatorPreCompressionData, mesh, 1.e-3)


    print("CompressOperator done")

    SIO.SaveState("collectionProblemData", collectionProblemData)
    SIO.SaveState("snapshotCorrelationOperator", snapshotCorrelationOperator)


    #################################################################
    ### ONLINE
    #################################################################

    collectionProblemData = SIO.LoadState("collectionProblemData")
    operatorCompressionData = collectionProblemData.GetOperatorCompressionData("T")
    reducedOrderBases = collectionProblemData.GetReducedOrderBases()

    snapshotCorrelationOperator = SIO.LoadState("snapshotCorrelationOperator")



    folder = GetTestDataPath() + "Zset/ThermalSequential/"

    inputFileName = folder + "square.inp"
    inputReader = ZIR.ZsetInputReader(inputFileName)

    meshFileName = folder + "square.geof"
    mesh = ZMR.ReadMesh(meshFileName)

    onlineProblemData = ProblemData.ProblemData("Online")
    onlineProblemData.SetDataFolder(os.path.relpath(folder))

    timeSequence = inputReader.ReadInputTimeSequence()

    constitutiveLawsList = inputReader.ConstructConstitutiveLawsList()
    onlineProblemData.AddConstitutiveLaw(constitutiveLawsList)

    loadingList = inputReader.ConstructLoadingsList()
    onlineProblemData.AddLoading(loadingList)
    for loading in onlineProblemData.GetLoadingsForSolution("T"):
        loading.ReduceLoading(mesh, onlineProblemData, reducedOrderBases, operatorCompressionData)


    initialCondition = inputReader.ConstructInitialCondition()
    initialCondition.ReduceInitialSnapshot(reducedOrderBases, snapshotCorrelationOperator)

    onlineProblemData.SetInitialCondition(initialCondition)

    onlineCompressedSolution = Th.ComputeOnline(onlineProblemData, timeSequence, operatorCompressionData, 1.e-4)

    class callback():
        def CurrentTime(timeStep, time):
            print("time =", time)
        def CurrentNormRes(normRes):
            print("normRes  =", normRes)
        def CurrentNewtonIterations(count):
            print("=== Newton iterations:", count)

    onlineCompressedSolution = Th.ComputeOnline(onlineProblemData, timeSequence, operatorCompressionData, 1.e-4, callback = callback)

    print("check T")
    ROMErrors = []
    for time in onlineCompressedSolution.keys():
        exact = solutionT.GetSnapshotAtTime(time)
        normExact = np.linalg.norm(exact)
        reconstructed = np.dot(reducedOrderBases["T"].T, onlineCompressedSolution[time])
        relError = np.linalg.norm(reconstructed - exact)
        if normExact > 0:
            relError /= normExact
        ROMErrors.append(relError)

    print("ROMErrors = ", ROMErrors)


    os.system("rm -rf collectionProblemData.pkl")
    os.system("rm -rf snapshotCorrelationOperator.pkl")


    return "ok"

if __name__ == "__main__":
    print(test())  # pragma: no cover

