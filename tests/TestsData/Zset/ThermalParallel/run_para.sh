#!/bin/sh


unset I_MPI_PMI_LIBRARY
unset MKL_NUM_THREADS
export OMP_NUM_THREADS=1
export MKL_DYNAMIC=FALSE
export OMP_DYNAMIC=FALSE

mpirun -n 2 $Z7PATH/calcul/Zebulon_cpp_Linux_64 -mpimpi . cube
