# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


import numpy as np
from genericROM.BasicAlgorithms import DataVisualization as DV
from genericROM.BasicAlgorithms import Clustering
from scipy.spatial import distance_matrix



def test():

    # Generate dataset
    n1 = 20
    n2 = 20
    n3 = 30
    mean1 = np.asarray([8.,12., 0.])
    mean2 = np.asarray([0.,10., 3.])
    mean3 = np.asarray([5.,0., 1.])
    cov = np.diag(np.ones(3), k=0)
    dataset1 = np.random.multivariate_normal(mean1, cov, size = n1)
    dataset2 = np.random.multivariate_normal(mean2, cov, size = n2)
    dataset3 = np.random.multivariate_normal(mean3, cov, size = n3)
    dataset = np.concatenate((dataset1, dataset2, dataset3), axis = 0)
    distanceMatrix = distance_matrix(dataset, dataset)

    # k-medoids clustering
    clusteringAlgo = Clustering.KMedoids(nClusters=3, nIter=100, init='k-meds++')
    cluster        = Clustering.ClusteringToolbox()
    cluster.SetClusteringAlgo(clusteringAlgo)
    cluster.fit_predict(distanceMatrix)
    medoids  = cluster.GetClusteringAlgo().GetMedoids()
    clusters = cluster.GetClusters()

    # MDS visu in 2D
    visu = DV.VisualizationToolbox(method='MDS', dissimilarity='precomputed')
    visu.fit_transform(distanceMatrix, dimension=2, outputName='embeddedData')
    visu.LoadEmbeddedData('embeddedData.npy')
    visu.SetEmbeddedData(visu.GetEmbeddedData())
    visu.PlotEmbeddedData("MDS_2D", ['z1','z2'])
    visu.PlotClusteringResultsOnEmbeddedData("clustering_MDS_2D", clusters)
    visu.PlotClusteringResultsAndSpecificPointsOnEmbeddedData("MDS_2D+spPoints", clusters, medoids)

    # MDS visu in 3D
    visu = DV.VisualizationToolbox(method='MDS', dissimilarity='precomputed')
    visu.fit(distanceMatrix, dimension=3)
    visu.PlotEmbeddedData("MDS_3D", ['z1','z2','z3'])
    visu.PlotClusteringResultsOnEmbeddedData("clustering_MDS_3D", clusters)
    visu.PlotClusteringResultsAndSpecificPointsOnEmbeddedData("clustering+spPoints_MDS_3D", clusters, medoids)

    # t-SNE visu in 2D
    visu = DV.VisualizationToolbox(method='t-SNE', dissimilarity='euclidean')
    visu.SetTSNEPerplexity(1.)
    visu.fit(dataset, dimension=2)
    visu.PlotEmbeddedData("t-SNE_2D", ['z1','z2'])


    import os
    os.system("rm MDS_2D.png clustering_MDS_2D.png MDS_2D+spPoints.png MDS_3D.png")
    os.system("rm clustering_MDS_3D.png clustering+spPoints_MDS_3D.png t-SNE_2D.png embeddedData.npy")

    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
