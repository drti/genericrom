# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.BasicAlgorithms import Clustering as C

from scipy.spatial import distance_matrix
from Muscat.Helpers.IO.TemporaryDirectory import TemporaryDirectory
import numpy as np
import os



def test():

    curDir = os.getcwd()
    tempdir = TemporaryDirectory.GetTempPath()
    os.chdir(tempdir)

    n1 = 60
    n2 = 60
    n3 = 80
    mean1 = np.asarray([8.,12.])
    mean2 = np.asarray([0.,10.])
    mean3 = np.asarray([5.,0.])
    cov = np.diag(np.ones(2), k=0)
    dataset1 = np.random.multivariate_normal(mean1, cov, size = n1)
    dataset2 = np.random.multivariate_normal(mean2, cov, size = n2)
    dataset3 = np.random.multivariate_normal(mean3, cov, size = n3)
    dataset = np.concatenate((dataset1, dataset2, dataset3), axis = 0)
    distanceMatrix = distance_matrix(dataset, dataset)

    clusteringAlgo = C.KMedoids(nClusters=3, nIter=100, init='k-meds++')
    cluster        = C.ClusteringToolbox()
    cluster.SetClusteringAlgo(clusteringAlgo)
    cluster.fit_predict(distanceMatrix, returnLabels=True, printCostFct=True)
    cluster.predictTest(distanceMatrix, returnLabels=True)
    cluster.WriteClusteringResults('clusteringResults_test.txt')
    cluster.GetClusteringAlgo().GetMedoids()
    clusters = cluster.GetClusters()
    cluster.ClusterRenumbering(np.arange(cluster.GetNumberOfClusters()))


    clusteringAlgo = C.KMedoids(nClusters=3, nIter=100, init='random', algo='ParkJun', squaredDist=False)
    cluster        = C.ClusteringToolbox()
    cluster.SetClusteringAlgo(clusteringAlgo)
    cluster.fit_predict(distanceMatrix, returnLabels=True, printCostFct=True)
    cluster.fit_predict(distanceMatrix, returnLabels=True, printCostFct=True, verbose = True)



    def Costfunction(medoids, distanceMatrix):
        labels = np.argmin(distanceMatrix[:,medoids], axis=1)
        cost = 0.
        for i in range(distanceMatrix.shape[0]):
            cost += distanceMatrix[i,medoids[labels[i]]]
        return cost

    clusteringAlgo = C.KMedoids(nClusters=3, nIter=100, init='random', algo='ParkJun', squaredDist=False)
    clusteringAlgo.SetCostFunction(Costfunction)
    cluster        = C.ClusteringToolbox()
    cluster.SetClusteringAlgo(clusteringAlgo)
    cluster.fit_predict(distanceMatrix, returnLabels=True, printCostFct=True)
    cluster.fit_predict(distanceMatrix, returnLabels=True, printCostFct=True, verbose = True)



    clusteringAlgo = C.KMedoids(nClusters=3, nIter=100, init='multipleRuns', algo='PAM', squaredDist=True)
    cluster        = C.ClusteringToolbox()
    cluster.SetClusteringAlgo(clusteringAlgo)
    cluster.fit(distanceMatrix, printCostFct=True)
    cluster.fit(distanceMatrix, printCostFct=True, verbose = True)
    cluster.predict(distanceMatrix, returnLabels=True)
    labels = cluster.GetLabels()
    C.GetAdjacentClustersFromLabelsVector(labels)
    C.GetAdjacentClustersFromLabelsVector(labels, [40, 100, 60])

    cluster2 = C.ClusteringToolbox()
    cluster2.ReadClusteringResults('clusteringResults_test.txt')
    K = cluster2.GetNumberOfClusters()
    clusters2 = cluster2.GetClusters()
    boolean = True
    for k in range(K):
        boolean = (boolean and (clusters[k]==clusters2[k]).all())
    if boolean:
        print("Writing/reading clustering results works.")
    else:
        print("Writing/reading clustering results does not work.")


    """import matplotlib.pyplot as plt
    from itertools import cycle, islice
    colors = np.array(list(islice(cycle(['#0000FF', '#008000', '#FF0000', '#FFA500', '#000080', '#FFD700', '#008B8B', '#32CD32', '#808080', '#F08080', '#8B4513', '#00FFFF',
                                         '#9400D3', '#FF1493','#DAA520', '#808000']),
                                         int(max(labels) + 1))))
    plt.figure()
    plt.title('Clusters')
    plt.scatter(dataset[:,0], dataset[:,1], s=10, color=colors[labels])
    plt.scatter(dataset[medoids,0], dataset[medoids,1], s=10, color='black')
    plt.xticks(())
    plt.yticks(())
    plt.show()"""

    os.system('rm clusteringResults_test.txt')
    os.chdir(curDir)

    return "ok"

if __name__ == "__main__":
    print(test())  # pragma: no cover