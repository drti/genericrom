# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.BasicAlgorithms import Metrics as M

import numpy as np
#import os


def test():

    numberOfSnapshots = 10
    numberOfDofs = 100

    snapshots = np.random.rand(numberOfSnapshots, numberOfDofs)

    M.SineDissimilarity(snapshots)
    M.L2Dissimilarity(snapshots)
    M.L2Dissimilarity(snapshots, verbose = True)
    M.L2Dissimilarity(snapshots, faster = False)
    M.L2Dissimilarity(snapshots, faster = False, verbose = True)
    M.ScalarProductXY(snapshots, snapshots)

    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
