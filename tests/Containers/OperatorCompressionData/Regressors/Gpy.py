# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import numpy as np
from genericROM.Containers.OperatorCompressionData.Regressors import Gpy as G
import pytest


@pytest.mark.skip(reason="GPy dependency bugged")
def test():


    one2 = np.ones(2)
    one4 = np.ones(4)

    X = np.array([one2, 2.*one2, 3.*one2, 7.*one2, one2, 2.*one2, 3.*one2, 9.*one2])
    y = np.array([one4, 2.*one4, 3.*one4, 7.*one4, one4, 2.*one4, 3.*one4, 9.*one4])

    options = {"kernel":"Matern52", "num_restarts":1}

    regressor = G.Gpy("U", options)

    regressor.Fit(X,y)

    yPred = regressor.Predict(X[:2])

    yPredRef = np.array(y[:2])

    np.testing.assert_almost_equal(yPred, yPredRef, decimal=4)

    print(regressor)

    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
