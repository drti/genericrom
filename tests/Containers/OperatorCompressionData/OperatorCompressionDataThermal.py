# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.Containers.OperatorCompressionData import OperatorCompressionDataThermal as OCDT
from genericROM.Containers.OperatorCompressionData import OperatorPreCompressionDataThermal as OPCDT
import numpy as np



def test():


    gradPhiAtIntegPoint = [np.arange(12).reshape(3,4),np.arange(12).reshape(3,4)]
    phiAtIntegPoint = np.arange(12).reshape(3,4)
    integrationWeights = np.ones(3)
    integrationWeightsRadiation = np.ones(4)
    phiAtIntegPointRadiationSet = np.arange(15).reshape(3,5)
    listOfTags = [["A"],["A", "B"],["B"]]

    operatorPreCompressionDataThermal = OPCDT.OperatorPreCompressionDataThermal("T", gradPhiAtIntegPoint, phiAtIntegPoint, integrationWeights, \
                 listOfTags, integrationWeightsRadiation, phiAtIntegPointRadiationSet)

    operatorCompressionDataThermal = OCDT.OperatorCompressionDataThermal("T")
    operatorCompressionDataThermal.SetOperatorPreCompressionData(operatorPreCompressionDataThermal)


    assert operatorCompressionDataThermal.GetListOfTags() == [["A"],["A", "B"],["B"]]
    assert operatorCompressionDataThermal.GetNumberOfIntegrationPoints() == 3
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetGradPhiAtIntegPoint(), [np.arange(12).reshape(3,4),np.arange(12).reshape(3,4)])
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetIntegrationWeights(), np.ones(3))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetPhiAtIntegPoint(), np.arange(12).reshape(3,4))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetIntegrationWeightsRadiation(), np.ones(4))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetPhiAtIntegPointRadiationSet(), np.arange(15).reshape(3,5))


    #radiation
    operatorCompressionDataThermal.SetReducedIntegrationPointsRadiation(np.arange(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationPointsRadiation(), np.arange(2))

    operatorCompressionDataThermal.SetReducedIntegrationWeightsRadiation(1.+np.ones(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationWeightsRadiation(), 1.+np.ones(2))

    operatorCompressionDataThermal.SetReducedPhiAtRadReducedIntegPoint(np.arange(6).reshape(2,3))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedPhiAtRadReducedIntegPoint(), np.arange(6).reshape(2,3))

    operatorCompressionDataThermal.SetReducedPhiPhiAtRadReducedIntegPoint(np.arange(24).reshape(2,3,4))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedPhiPhiAtRadReducedIntegPoint(), np.arange(24).reshape(2,3,4))


    #capacity
    operatorCompressionDataThermal.SetReducedIntegrationPointsCapacity(np.arange(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationPointsCapacity(), np.arange(2))

    operatorCompressionDataThermal.SetReducedIntegrationWeightsCapacity(1.+np.ones(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationWeightsCapacity(), 1.+np.ones(2))

    operatorCompressionDataThermal.SetReducedPhiAtCapacityReducedIntegPoint(np.arange(6).reshape(2,3))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedPhiAtCapacityReducedIntegPoint(), np.arange(6).reshape(2,3))

    operatorCompressionDataThermal.SetReducedPhiPhiAtCapacityReducedIntegPoint(np.arange(24).reshape(2,3,4))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedPhiPhiAtCapacityReducedIntegPoint(), np.arange(24).reshape(2,3,4))

    operatorCompressionDataThermal.SetReducedListOTagsCapacity([["C"],["D", "B"],["B"]])
    assert operatorCompressionDataThermal.GetReducedListOTagsCapacity() == [["C"],["D", "B"],["B"]]


    #conductivity
    operatorCompressionDataThermal.SetReducedIntegrationPointsConductivity(np.arange(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationPointsConductivity(), np.arange(2))

    operatorCompressionDataThermal.SetReducedIntegrationWeightsConductivity(1.+np.ones(2))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedIntegrationWeightsConductivity(), 1.+np.ones(2))

    operatorCompressionDataThermal.SetReducedPhiAtConductivityReducedIntegPoint(np.arange(6).reshape(2,3))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedPhiAtConductivityReducedIntegPoint(), np.arange(6).reshape(2,3))

    operatorCompressionDataThermal.SetReducedGradGradAtConductivityReducedIntegPoint(np.arange(24).reshape(2,3,4))
    np.testing.assert_almost_equal(operatorCompressionDataThermal.GetReducedGradGradAtConductivityReducedIntegPoint(), np.arange(24).reshape(2,3,4))

    operatorCompressionDataThermal.SetReducedListOTagsConductivity([["C"],["D", "F"],["B"]])
    assert operatorCompressionDataThermal.GetReducedListOTagsConductivity() == [["C"],["D", "F"],["B"]]


    operatorCompressionDataThermal.__getstate__()

    print(operatorCompressionDataThermal)
    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
