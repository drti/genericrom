# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.Containers.OperatorCompressionData import OperatorPreCompressionDataThermal as OPCDT

import numpy as np


def test():

    gradPhiAtIntegPoint = [np.arange(12).reshape(3,4),np.arange(12).reshape(3,4)]
    phiAtIntegPoint = np.arange(12).reshape(3,4)
    integrationWeights = np.ones(3)
    integrationWeightsRadiation = np.ones(4)
    phiAtIntegPointRadiationSet = np.arange(15).reshape(3,5)
    listOfTags = [["A"],["A", "B"],["B"]]

    operatorPreCompressionDataThermal = OPCDT.OperatorPreCompressionDataThermal("T", gradPhiAtIntegPoint, phiAtIntegPoint, integrationWeights, \
                 listOfTags, integrationWeightsRadiation, phiAtIntegPointRadiationSet)


    assert operatorPreCompressionDataThermal.GetListOfTags() == [["A"],["A", "B"],["B"]]
    assert operatorPreCompressionDataThermal.GetNumberOfIntegrationPoints() == 3
    np.testing.assert_almost_equal(operatorPreCompressionDataThermal.GetGradPhiAtIntegPoint(), [np.arange(12).reshape(3,4),np.arange(12).reshape(3,4)])
    np.testing.assert_almost_equal(operatorPreCompressionDataThermal.GetIntegrationWeights(), np.ones(3))
    np.testing.assert_almost_equal(operatorPreCompressionDataThermal.GetPhiAtIntegPoint(), np.arange(12).reshape(3,4))
    np.testing.assert_almost_equal(operatorPreCompressionDataThermal.GetIntegrationWeightsRadiation(), np.ones(4))
    np.testing.assert_almost_equal(operatorPreCompressionDataThermal.GetPhiAtIntegPointRadiationSet(), np.arange(15).reshape(3,5))


    operatorPreCompressionDataThermal.__getstate__()

    print(operatorPreCompressionDataThermal)
    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover


