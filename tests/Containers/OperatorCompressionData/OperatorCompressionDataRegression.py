# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.Containers.OperatorCompressionData import OperatorCompressionDataRegression as OCDR
#import numpy as np



def test():

    operatorCompressionDataMechanical = OCDR.OperatorCompressionDataRegression("U")
    operatorCompressionDataMechanical.SetModel("model1")
    operatorCompressionDataMechanical.SetScalerParameters("A")
    operatorCompressionDataMechanical.SetScalerCoefficients("B")

    operatorCompressionDataMechanical.GetModel()
    operatorCompressionDataMechanical.GetScalerParameters()
    operatorCompressionDataMechanical.GetScalerCoefficients()


    print(operatorCompressionDataMechanical)
    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover


