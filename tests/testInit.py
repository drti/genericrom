# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM import GetTestDataPath, GetTestPath, GetExamplesPath, RunTestFile
import os

def test():

    print("tests path is :", GetTestPath())
    print("TestsData path is :", GetTestDataPath())
    print("examples path is :", GetExamplesPath())

    return "ok"

if __name__ == "__main__":
    print(test())  # pragma: no cover
