# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from genericROM.IO import ZsetSolutionReader as ZSR
from genericROM import GetTestDataPath, GetTestPath
from Mordicus.IO import StateIO as SIO
import os
import numpy as np


def test():

    folder = GetTestDataPath() + "Zset"+os.sep+"MecaSequential"+os.sep

    solutionFileName = folder + "cube.ut"

    reader = ZSR.ZsetSolutionReader(solutionFileName)

    ref = SIO.LoadState(GetTestPath()+"IO"+os.sep+"ref"+os.sep+"ZsetSolutionReader", extension = 'res')

    res = []
    res.append(reader.ReadSnapshotComponent("U1", 1.0, primality=True))
    ZSR.ReadSnapshotComponent(solutionFileName, "U1", 1.0, primality=True)

    res.append(reader.ReadTimeSequenceFromSolutionFile())
    ZSR.ReadTimeSequenceFromSolutionFile(solutionFileName)

    timeSequence2 = [0.0, 0.2, 0.8, 1.0, 1.4, 1.6, 2.0]
    res.append(reader.ReadSnapshotComponentTimeSequence("U1", timeSequence2, primality=True))

    # SIO.SaveState(GetTestPath()+"IO"+os.sep+"ref"+os.sep+"ZsetSolutionReader", res, extension = "res")

    for i in range(len(res)):
        np.testing.assert_almost_equal(res[i], ref[i])


    return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
