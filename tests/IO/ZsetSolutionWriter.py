# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


import numpy as np
import os
from Mordicus.Containers import Solution
from Mordicus.Containers import ProblemData
from Mordicus.Containers import CollectionProblemData as CPD
from genericROM.IO import ZsetSolutionWriter as ZSW
from genericROM.IO import ZsetMeshReader as ZMR
from genericROM.FE import FETools as FE
from genericROM import GetTestDataPath, GetTestPath
import filecmp

def test():

      meshFileName = GetTestDataPath() + "Zset/MecaSequential/cube.geof"
      mesh = ZMR.ReadMesh(meshFileName)

      Nnode = mesh.GetInternalStorage().GetNumberOfNodes()
      dim = mesh.GetInternalStorage().GetPointsDimensionality()
      Ninteg = FE.ComputeNumberOfIntegrationPoints(mesh)

      collectionProblemData = CPD.CollectionProblemData()
      collectionProblemData.AddVariabilityAxis("config", str)
      collectionProblemData.DefineQuantity("U")
      collectionProblemData.AddReducedOrderBasis("U", np.arange(2*dim*Nnode).reshape((2,dim*Nnode)))
      collectionProblemData.DefineQuantity("p")
      collectionProblemData.AddReducedOrderBasis("p", np.arange(3*Ninteg).reshape(3,Ninteg))

      solutionU = Solution.Solution("U", dim, Nnode, True)
      solutionU.AddReducedCoordinates(np.ones(2), 0.)
      solutionU.AddReducedCoordinates(0.5+np.ones(2), 1.)
      solutionP = Solution.Solution("p", 1, Ninteg, False)
      solutionP.AddReducedCoordinates(np.ones(3), 0.)
      solutionP.AddReducedCoordinates(0.5+np.ones(3), 1.)

      problemData = ProblemData.ProblemData("myProblem")
      problemData.AddSolution(solutionU)
      problemData.AddSolution(solutionP)


      folder = GetTestPath() + 'IO/'

      ZSW.WriteZsetSolution(mesh, os.path.relpath(meshFileName, start=folder), folder+'ZsetSolutionWriter1',\
         collectionProblemData, problemData, "U")

      ZSW.WriteZsetSolution(mesh, os.path.relpath(meshFileName, start=folder), folder+'ZsetSolutionWriter2',\
         collectionProblemData, problemData, "U", outputReducedOrderBasis = True)


      with open(folder+"ZsetSolutionWriter1.ut") as f:
         geofLink = f.readline().rstrip().split()[1]
      with open(folder+"ref/ZsetSolutionWriter1.ut") as f:
         geofRefLink = f.readline().rstrip().split()[1]

      if geofLink == geofRefLink:
         assert filecmp.cmp(folder+"ZsetSolutionWriter1.ut",   folder+"ref/ZsetSolutionWriter1.ut",   shallow=False) == True
         assert filecmp.cmp(folder+"ZsetSolutionWriter1.integ",folder+"ref/ZsetSolutionWriter1.integ",shallow=False) == True
         assert filecmp.cmp(folder+"ZsetSolutionWriter1.node", folder+"ref/ZsetSolutionWriter1.node", shallow=False) == True
         assert filecmp.cmp(folder+"ZsetSolutionWriter2.ut",   folder+"ref/ZsetSolutionWriter2.ut",   shallow=False) == True
         assert filecmp.cmp(folder+"ZsetSolutionWriter2.integ",folder+"ref/ZsetSolutionWriter2.integ",shallow=False) == True
         assert filecmp.cmp(folder+"ZsetSolutionWriter2.node", folder+"ref/ZsetSolutionWriter2.node", shallow=False) == True

      os.system("rm -rf "+folder+"ZsetSolutionWriter1.ut "+folder+"ZsetSolutionWriter1.integ "+folder+"ZsetSolutionWriter1.node")
      os.system("rm -rf "+folder+"ZsetSolutionWriter2.ut "+folder+"ZsetSolutionWriter2.integ "+folder+"ZsetSolutionWriter2.node")


      return "ok"


if __name__ == "__main__":
    print(test())  # pragma: no cover
