# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from setuptools import setup, Command
from distutils.command.build import build
import os
from glob import glob
from pathlib import Path

class CompileCommand(Command):
    description = "Compile pyumat for Z-mat constitutive laws"
    user_options = []
    def initialize_options(self):
        self.cwd = None
    def finalize_options(self):
        self.cwd = os.getcwd()
    def run(self):
        assert os.getcwd() == self.cwd, 'Must be in package root: %s' % self.cwd
        os.system('make -C src/genericROM/External/pyumat')


class my_build(build):
    def run(self):
        self.run_command("compile")
        build.run(self)


fileList = glob('tests/**/*.*', recursive=True) + glob('examples/**/*.*', recursive=True)
FolderList = [str(Path(file).parents[0]) for file in fileList]

setup(
    cmdclass={ 'build': my_build, 'compile': CompileCommand},
    data_files=[('ressources/genericROM/'+folder, [file]) for (folder,file) in zip(FolderList,fileList)]
)
