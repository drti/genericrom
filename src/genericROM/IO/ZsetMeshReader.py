# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.IO.MeshReaderBase import MeshReaderBase
try:
    from mpi4py import MPI
except ImportError:# pragma: no cover
    print("MPI capabilities not available")
from pathlib import Path
import os

def ReadMesh(meshFileName):
    """
    Functional API

    Reads the mesh defined the Z-set mesh file "meshFileName" (.geof or .geo)

    Parameters
    ----------
    meshFileName : str
        Z-set mesh file

    Returns
    -------
    MuscatMesh
        high-dimensional mesh
    """
    reader = ZsetMeshReader(meshFileName=meshFileName)
    return reader.ReadMesh()


class ZsetMeshReader(MeshReaderBase):
    """
    Class containing a reader for Z-set mesh file

    Attributes
    ----------
    meshFileName : str
        name of the Z-set mesh file (.geof or .geo)
    reader : GeoReader or GeofReader
        Muscat reader of .geof or .geo files
    """

    def __init__(self, meshFileName):
        assert isinstance(meshFileName, str)

        super(ZsetMeshReader, self).__init__()

        folder = str(Path(meshFileName).parents[0])
        suffix = str(Path(meshFileName).suffix)
        stem = str(Path(meshFileName).stem)


        if MPI.COMM_WORLD.Get_size() > 1: # pragma: no cover
            meshFileName = folder + os.sep + stem + "-pmeshes" + os.sep + stem + "-" + str(MPI.COMM_WORLD.Get_rank()+1).zfill(3) + suffix
        else:
            meshFileName = meshFileName


        if suffix == ".geof":
            from Muscat.IO import GeofReader as GR
            self.reader = GR.GeofReader()

        elif suffix == ".geo":  # pragma: no cover
            from Muscat.IO import GeoReader as GR
            self.reader = GR.GeoReader()

        else:  # pragma: no cover
            raise NotImplementedError("meshFileName error!")

        self.reader.SetFileName(meshFileName)



    def ReadMesh(self):
        """
        Reads the high-dimensional mesh

        Returns
        -------
        MuscatMesh
            high-dimensional mesh
        """
        data = self.reader.Read()

        from genericROM.Containers.Meshes import MuscatUnstructuredMesh as MUM

        mesh = MUM.MuscatUnstructuredMesh(data)

        return mesh


if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)
