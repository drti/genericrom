# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import os
from pathlib import Path

def GetTestPath():
    """
    Help function to access the tests path of the library
    """
    if "GENERICROMTESTPATH" in os.environ:# pragma: no cover
        return os.getenv('GENERICROMTESTPATH') + os.sep
    else:
        return os.path.normpath(str(Path(os.path.realpath(__file__)).parents[2]) + os.sep + "tests") + os.sep


def GetExamplesPath():
    """
    Help function to access the tests path of the library
    """
    if "GENERICROMEXAMPLESPATH" in os.environ:# pragma: no cover
        return os.getenv('GENERICROMEXAMPLESPATH') + os.sep
    else:
        return os.path.normpath(str(Path(os.path.realpath(__file__)).parents[2]) + os.sep + "examples") + os.sep


def GetTestDataPath():
    """
    Help function to access the TestsData path of the library
    """
    if "GENERICROMTESTDATAPATH" in os.environ:# pragma: no cover
        return os.getenv('GENERICROMTESTDATAPATH') + os.sep
    else:
        return os.path.normpath(str(Path(os.path.realpath(__file__)).parents[2]) + os.sep + "tests" + os.sep + "TestsData") + os.sep


def RunTestFile(srcFilePath):# pragma: no cover
    """
    Help function to run the tests corresponding to a source file
    """
    testFolder = GetTestPath()

    relPath = os.path.relpath(os.path.realpath(srcFilePath), testFolder)
    testFile = os.path.join(testFolder, os.path.relpath(relPath, os.path.join("..", "src", "genericROM")))
    folderFile = os.path.dirname(testFile)

    os.chdir(folderFile)

    os.system("python "+testFile)