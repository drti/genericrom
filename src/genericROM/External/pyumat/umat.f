      subroutine getoutdir(OUTDIR, LENOUTDIR)
      LENOUTDIR = 0
      return
      end

      subroutine umat(stress,statev,ddsdde,sse,spd,scd,
     1 rpl,ddsddt,drplde,drpldt,stran,dstran,
     2 time,dtime,temp,dtemp,predef,dpred,cmname,ndi,nshr,
     3 ntens,nstatv,props,nprops,coords,drot,pnewdt,celent,
     4 dfgrd0,dfgrd1,noel,npt,kslay,kspt,kstep,kinc)

      implicit real*8(a-h,o-z)
      parameter (nprecd=2)

      character*8 cmname
      dimension stress(ntens),statev(nstatv),
     1 ddsdde(ntens,ntens),ddsddt(ntens),drplde(ntens),
     2 stran(ntens),dstran(ntens),time(2),predef(1),dpred(1),
     3 props(nprops),coords(3),drot(3,3),
     4 dfgrd0(3,3),dfgrd1(3,3)
      common/count/kiter,kitgen

      call zebaba(stress,statev,ddsdde,sse,spd,scd,
     1  rpl,ddsddt,drplde,drpldt,
     2  stran,dstran,time,dtime,temp,dtemp,predef,dpred,cmname,
     3  ndi,nshr,ntens,nstatv,props,nprops,coords,drot,
     4  pnewdt,celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,kstep,
     5  kinc,kiter,kitgen)
      return
      end
