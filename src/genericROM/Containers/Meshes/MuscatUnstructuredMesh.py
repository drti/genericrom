# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.Meshes.MeshBase import MeshBase


class MuscatUnstructuredMesh(MeshBase):
    """
    Class containing a wrapper for the format Muscat.Containers.Mesh

    Attributes
    ----------
    __storage : Muscat.Containers.Mesh
    """

    def __init__(self, mesh):
        """
        Parameters
        ----------
        mesh : Muscat.Containers.Mesh
            mesh of the high-fidelity model
        """
        super(MuscatUnstructuredMesh, self).__init__()
        from Muscat.Containers import Mesh as MuscatMesh

        assert isinstance(mesh, MuscatMesh.Mesh)

        self.SetInternalStorage(mesh)


    def GetNodes(self):
        """
        Returns the nodes of the mesh

        Returns
        -------
        np.ndarray
            of size (numberOfNodes,dimensionality)
        """
        return self.GetInternalStorage().nodes


    def AllElementsIterator(self):
        """
        Constructs an iterator over the elements of the mesh

        Returns
        -------
        iterator
            an iterator over the elements of the mesh
        """
        class iterator:
            def __init__(self, elements):
                self.elements = elements

            def __iter__(self):
                for _, data in self.elements.items():
                    for i in range(data.GetNumberOfElements()):
                        yield data.connectivity[i, :]

        res = iterator(self.GetInternalStorage().elements)
        return res


    def __str__(self):
        res = str(self.GetInternalStorage())
        return res


if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)
