# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.OperatorCompressionData.OperatorCompressionDataBase import OperatorCompressionDataBase



class OperatorCompressionDataThermal(OperatorCompressionDataBase):
    """
    Class containing an OperatorCompressionDataThermal, used in the
    operator compression step of the POD-ECM method. The implementation
    uses Lagrange isoparametric finite elements as high-dimension integration
    model.

    Attributes
    ----------
    """

    def __init__(self, solutionName):
        super(OperatorCompressionDataThermal, self).__init__(solutionName)

        self.gradPhiAtIntegPoint         = None
        self.phiAtIntegPoint             = None
        self.integrationWeights          = None

        self.listOfTags                  = None
        self.integrationWeightsRadiation = None
        self.phiAtIntegPointRadiationSet = None


        #radiation
        self.reducedIntegrationPointsRadiation   = None
        self.reducedIntegrationWeightsRadiation  = None
        self.reducedPhiAtRadReducedIntegPoint    = None
        self.reducedPhiPhiAtRadReducedIntegPoint = None


        #capacity
        self.reducedIntegrationPointsCapacity         = None
        self.reducedIntegrationWeightsCapacity        = None
        self.reducedPhiAtCapacityReducedIntegPoint    = None
        self.reducedPhiPhiAtCapacityReducedIntegPoint = None
        self.reducedListOTagsCapacity                 = None


        #conductivity
        self.reducedIntegrationPointsConductivity           = None
        self.reducedIntegrationWeightsConductivity          = None
        self.reducedPhiAtConductivityReducedIntegPoint      = None
        self.reducedGradGradAtConductivityReducedIntegPoint = None
        self.reducedListOTagsConductivity                   = None



    def SetOperatorPreCompressionData(self, operatorPreCompressionDataThermal):
        """
        Sets the gradPhiAtIntegPoint, integrationWeights and listOfTags
        attributes from an OperatorPreCompressionDataThermal

        Parameters
        ----------
        OperatorPreCompressionDataThermal : OperatorPreCompressionDataThermal
            data structure used in a precomputation of the operator compression step of the POD-ECM method
        """
        assert self.GetSolutionName() == operatorPreCompressionDataThermal.GetSolutionName(), \
        "solutionName of OperatorCompressionDataThermal and OperatorPreCompressionDataThermal must be the same"

        self.gradPhiAtIntegPoint = operatorPreCompressionDataThermal.GetGradPhiAtIntegPoint()
        self.phiAtIntegPoint = operatorPreCompressionDataThermal.GetPhiAtIntegPoint()
        self.integrationWeights = operatorPreCompressionDataThermal.GetIntegrationWeights()

        self.listOfTags = operatorPreCompressionDataThermal.GetListOfTags()
        self.integrationWeightsRadiation = operatorPreCompressionDataThermal.GetIntegrationWeightsRadiation()
        self.phiAtIntegPointRadiationSet = operatorPreCompressionDataThermal.GetPhiAtIntegPointRadiationSet()


    def GetIntegrationWeights(self):
        """
        Get the integrationWeights attribute

        Returns
        -------
        1D np.ndarray
            of size (numberOfIntegrationPoints,)
        """
        return self.integrationWeights


    def GetNumberOfIntegrationPoints(self):
        """
        Get the number of integration points

        Returns
        -------
        int
            number of integration points
        """
        return len(self.integrationWeights)


    def GetGradPhiAtIntegPoint(self):
        """
        Get the gradPhiAtIntegPoint attribute

        Returns
        -------
        list
            of length dimensionality of the mesh, of scipy.sparse.coo_matrix of
            size (numberOfIntegrationPoints, numberOfModes)
        """
        return self.gradPhiAtIntegPoint


    def GetPhiAtIntegPoint(self):
        """
        Get the phiAtIntegPoint attribute

        Returns
        -------
        scipy.sparse.coo_matrix
            of size (numberOfIntegrationPoints, numberOfModes)
        """
        return self.phiAtIntegPoint


    def GetListOfTags(self):
        return self.listOfTags


    def GetIntegrationWeightsRadiation(self):
        return self.integrationWeightsRadiation


    def GetPhiAtIntegPointRadiationSet(self):
        return self.phiAtIntegPointRadiationSet


    #radiation

    def SetReducedIntegrationPointsRadiation(self, reducedIntegrationPointsRadiation):
        self.reducedIntegrationPointsRadiation = reducedIntegrationPointsRadiation

    def GetReducedIntegrationPointsRadiation(self):
        return self.reducedIntegrationPointsRadiation

    def SetReducedIntegrationWeightsRadiation(self, reducedIntegrationWeightsRadiation):
        self.reducedIntegrationWeightsRadiation = reducedIntegrationWeightsRadiation

    def GetReducedIntegrationWeightsRadiation(self):
        return self.reducedIntegrationWeightsRadiation

    def SetReducedPhiAtRadReducedIntegPoint(self, reducedPhiAtRadReducedIntegPoint):
        self.reducedPhiAtRadReducedIntegPoint = reducedPhiAtRadReducedIntegPoint

    def GetReducedPhiAtRadReducedIntegPoint(self):
        return self.reducedPhiAtRadReducedIntegPoint

    def SetReducedPhiPhiAtRadReducedIntegPoint(self, reducedPhiPhiAtRadReducedIntegPoint):
        self.reducedPhiPhiAtRadReducedIntegPoint = reducedPhiPhiAtRadReducedIntegPoint

    def GetReducedPhiPhiAtRadReducedIntegPoint(self):
        return self.reducedPhiPhiAtRadReducedIntegPoint


    #capacity

    def SetReducedIntegrationPointsCapacity(self, reducedIntegrationPointsCapacity):
        self.reducedIntegrationPointsCapacity = reducedIntegrationPointsCapacity

    def GetReducedIntegrationPointsCapacity(self):
        return self.reducedIntegrationPointsCapacity

    def SetReducedIntegrationWeightsCapacity(self, reducedIntegrationWeightsCapacity):
        self.reducedIntegrationWeightsCapacity = reducedIntegrationWeightsCapacity

    def GetReducedIntegrationWeightsCapacity(self):
        return self.reducedIntegrationWeightsCapacity

    def SetReducedPhiAtCapacityReducedIntegPoint(self, reducedPhiAtCapacityReducedIntegPoint):
        self.reducedPhiAtCapacityReducedIntegPoint = reducedPhiAtCapacityReducedIntegPoint

    def GetReducedPhiAtCapacityReducedIntegPoint(self):
        return self.reducedPhiAtCapacityReducedIntegPoint

    def SetReducedPhiPhiAtCapacityReducedIntegPoint(self, reducedPhiPhiAtCapacityReducedIntegPoint):
        self.reducedPhiPhiAtCapacityReducedIntegPoint = reducedPhiPhiAtCapacityReducedIntegPoint

    def GetReducedPhiPhiAtCapacityReducedIntegPoint(self):
        return self.reducedPhiPhiAtCapacityReducedIntegPoint

    def SetReducedListOTagsCapacity(self, reducedListOTagsCapacity):
        self.reducedListOTagsCapacity = reducedListOTagsCapacity

    def GetReducedListOTagsCapacity(self):
        return self.reducedListOTagsCapacity


    #conductivity

    def SetReducedIntegrationPointsConductivity(self, reducedIntegrationPointsConductivity):
        self.reducedIntegrationPointsConductivity = reducedIntegrationPointsConductivity

    def GetReducedIntegrationPointsConductivity(self):
        return self.reducedIntegrationPointsConductivity

    def SetReducedIntegrationWeightsConductivity(self, reducedIntegrationWeightsConductivity):
        self.reducedIntegrationWeightsConductivity = reducedIntegrationWeightsConductivity

    def GetReducedIntegrationWeightsConductivity(self):
        return self.reducedIntegrationWeightsConductivity

    def SetReducedPhiAtConductivityReducedIntegPoint(self, reducedPhiAtConductivityReducedIntegPoint):
        self.reducedPhiAtConductivityReducedIntegPoint = reducedPhiAtConductivityReducedIntegPoint

    def GetReducedPhiAtConductivityReducedIntegPoint(self):
        return self.reducedPhiAtConductivityReducedIntegPoint

    def SetReducedGradGradAtConductivityReducedIntegPoint(self, reducedGradGradAtConductivityReducedIntegPoint):
        self.reducedGradGradAtConductivityReducedIntegPoint = reducedGradGradAtConductivityReducedIntegPoint

    def GetReducedGradGradAtConductivityReducedIntegPoint(self):
        return self.reducedGradGradAtConductivityReducedIntegPoint

    def SetReducedListOTagsConductivity(self, reducedListOTagsConductivity):
        self.reducedListOTagsConductivity = reducedListOTagsConductivity

    def GetReducedListOTagsConductivity(self):
        return self.reducedListOTagsConductivity




    def __getstate__(self):

        state = {}
        state["gradPhiAtIntegPoint"] = None
        state["phiAtIntegPoint"] = None
        state["integrationWeights"] = None

        state["listOfTags"] = None
        state["integrationWeightsRadiation"] = None
        state["phiAtIntegPointRadiationSet"] = None

        #radiation
        state["reducedIntegrationPointsRadiation"] = self.reducedIntegrationPointsRadiation
        state["reducedIntegrationWeightsRadiation"] = self.reducedIntegrationWeightsRadiation
        state["reducedPhiAtRadReducedIntegPoint"] = self.reducedPhiAtRadReducedIntegPoint
        state["reducedPhiPhiAtRadReducedIntegPoint"] = self.reducedPhiPhiAtRadReducedIntegPoint

        #capacity
        state["reducedIntegrationPointsCapacity"] = self.reducedIntegrationPointsCapacity
        state["reducedIntegrationWeightsCapacity"] = self.reducedIntegrationWeightsCapacity
        state["reducedPhiAtCapacityReducedIntegPoint"] = self.reducedPhiAtCapacityReducedIntegPoint
        state["reducedPhiPhiAtCapacityReducedIntegPoint"] = self.reducedPhiPhiAtCapacityReducedIntegPoint
        state["reducedListOTagsCapacity"] = self.reducedListOTagsCapacity

        #conductivity
        state["reducedIntegrationPointsConductivity"] = self.reducedIntegrationPointsConductivity
        state["reducedIntegrationWeightsConductivity"] = self.reducedIntegrationWeightsConductivity
        state["reducedPhiAtConductivityReducedIntegPoint"] = self.reducedPhiAtConductivityReducedIntegPoint
        state["reducedGradGradAtConductivityReducedIntegPoint"] = self.reducedGradGradAtConductivityReducedIntegPoint
        state["reducedListOTagsConductivity"] = self.reducedListOTagsConductivity




        return state


    def __str__(self):
        res = "OperatorCompressionDataThermal"
        return res



if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)



