# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from genericROM.Containers.OperatorCompressionData.OperatorCompressionDataRegression import OperatorCompressionDataRegression


class Gpy(OperatorCompressionDataRegression):
    """
    Class containing a GPy OperatorCompressionData

    Attributes
    ----------
    options : dict
        options to pass to GPy
    model : GPy regressor
        Gaussian process regressor
    scalerX : sklearn.preprocessing._data.StandardScaler
        scaler for the input of the regression (the parameters of the problem)
    scalery : sklearn.preprocessing._data.StandardScaler
        scaler for the output of the regression (the coefficients on the
        reduced solution on the reducedOrderBasis - reducedCoordinates)
    """

    def __init__(self, solutionName, options = None):
        super(Gpy, self).__init__(solutionName)

        if options == None:# pragma: no cover
            options = {}
        chosen_options = {"kernel":"Matern52",
                          "optim":"bfgs",
                          "num_restarts":10,
                          "max_iters":1000,
                          "anisotrope":True}
        chosen_options.update(options)
        self.options = chosen_options
        self.model = None
        self.scalerX = None
        self.scalery = None


    def Fit(self, X, y):
        """
        Trains a GaussianProcessRegressor, using GPy, from training data and target values

        Parameters
        ----------
        X : np.ndarray
            training data
        y : np.ndarray
            target values

        Returns
        -------
        sklearn.model_selection._search.GridSearchCV
            trained and optimized scikit learn regressor
        sklearn.preprocessing._data.StandardScaler
            scaler trained on input data
        sklearn.preprocessing._data.StandardScaler
            scaler trained on output data
        """
        from sklearn import preprocessing

        self.scalerX = preprocessing.StandardScaler()#MinMaxScaler()#
        self.scalery = preprocessing.StandardScaler()#MinMaxScaler()#

        self.scalerX.fit(X)
        self.scalery.fit(y)

        X = self.scalerX.transform(X)
        y = self.scalery.transform(y)

        import GPy
        try:
            kernalClass = getattr(GPy.kern, self.options["kernel"])
        except AttributeError:# pragma: no cover
            raise
        k = kernalClass(input_dim=X.shape[1], ARD = self.options["anisotrope"])
        self.model = GPy.models.GPRegression(X, y, k)

        self.model.optimize_restarts(optimizer = self.options["optim"], max_iters = self.options["max_iters"], num_restarts = self.options["num_restarts"])

        print(self.model)


    def Predict(self, XTest):
        """
        Computes the prediction of the Regressor,taking into account prelearned scalers for input and output

        Parameters
        ----------
        XTest : np.ndarray
            testing data

        Returns
        -------
        np.ndarray
            kept eigenvalues, of size (numberOfEigenvalues)
        np.ndarray
            kept eigenvectors, of size (numberOfEigenvalues, numberOfSnapshots)
        """
        XTest = self.scalerX.transform(XTest)

        yTest, VyTest = self.model.predict(XTest)

        yTest = self.scalery.inverse_transform(yTest)

        return yTest


    def __str__(self):
        res = "GPy OperatorCompressionDataRegression\n"
        res += str(self.model)
        return res


if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)
