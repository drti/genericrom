# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.OperatorCompressionData.OperatorCompressionDataBase import OperatorCompressionDataBase



class OperatorPreCompressionDataThermal(OperatorCompressionDataBase):
    """
    Class containing an OperatorPreCompressionDataThermal, used in a precomputation
    of the operator compression step of the POD-ECM method. The implementation
    uses Lagrange isoparametric finite elements as high-dimension integration
    model. This structure only depends on the mesh, and is computed once
    in nongeometrical varitions cases, even when snapshots are updated.

    Attributes
    ----------
    gradPhiAtIntegPoint : list
        of length dimensionality of the mesh, of scipy.sparse.coo_matrix of
        size (numberOfIntegrationPoints, numberOfModes), components of the
        gradient of the shape functions at the integration points
    phiAtIntegPoint : scipy.sparse.coo_matrix
        of size (numberOfIntegrationPoints, numberOfModes), value of the finite
        element shape functions at the integration points and the integration
        weights (Lagrange isoparametric finite elements)
    integrationWeights : 1D np.ndarray
        of size (numberOfIntegrationPoints,), vector containing the integration
        weights associated to the computation of the internal forces vector
        with the high-fidelity integration scheme
    listOfTags : list of lists (of str)
        (of length numberOfIntegrationPoints) at each integration point,
        containing all the tags of the element containing the integration points
    integrationWeightsRadiation : 1D np.ndarray
        of size (numberOfRadiationIntegrationPoints,), vector containing the integration
        weights associated to the computation of the internal forces vector
        with the high-fidelity integration scheme
    phiAtIntegPointRadiationSet : scipy.sparse.coo_matrix
        of size (numberOfRadiationIntegrationPoints, numberOfModes), value of the finite
        element shape functions at the integration points and the integration
        weights (Lagrange isoparametric finite elements)
    """

    def __init__(self, solutionName, gradPhiAtIntegPoint, phiAtIntegPoint, integrationWeights, \
                 listOfTags, integrationWeightsRadiation, phiAtIntegPointRadiationSet):
        super(OperatorPreCompressionDataThermal, self).__init__(solutionName)

        self.gradPhiAtIntegPoint = gradPhiAtIntegPoint
        self.phiAtIntegPoint = phiAtIntegPoint
        self.integrationWeights = integrationWeights
        self.listOfTags = listOfTags
        self.integrationWeightsRadiation = integrationWeightsRadiation
        self.phiAtIntegPointRadiationSet = phiAtIntegPointRadiationSet


    def GetIntegrationWeights(self):
        """
        Get the integrationWeights attribute

        Returns
        -------
        1D np.ndarray
            of size (numberOfIntegrationPoints,)
        """
        return self.integrationWeights


    def GetNumberOfIntegrationPoints(self):
        """
        Get the number of integration points

        Returns
        -------
        int
            number of integration points
        """
        return len(self.integrationWeights)


    def GetGradPhiAtIntegPoint(self):
        """
        Get the gradPhiAtIntegPoint attribute

        Returns
        -------
        list
            of length dimensionality of the mesh, of scipy.sparse.coo_matrix of
            size (numberOfIntegrationPoints, numberOfModes)
        """
        return self.gradPhiAtIntegPoint


    def GetPhiAtIntegPoint(self):
        """
        Get the phiAtIntegPoint attribute

        Returns
        -------
        scipy.sparse.coo_matrix
            of size (numberOfIntegrationPoints, numberOfModes)
        """
        return self.phiAtIntegPoint


    def GetListOfTags(self):
        """
        Get the listOfTags attribute

        Returns
        -------
        list of lists (of str)
            (of length numberOfIntegrationPoints)
        """
        return self.listOfTags


    def GetIntegrationWeightsRadiation(self):
        """
        Get the number of integration points associated to the radiation term

        Returns
        -------
        int
            number of integration points associated to the radiation term
        """
        return self.integrationWeightsRadiation


    def GetPhiAtIntegPointRadiationSet(self):
        """
        Get the phiAtIntegPointRadiationSet attribute

        Returns
        -------
        scipy.sparse.coo_matrix
            of size (numberOfRadiationIntegrationPoints, numberOfModes)
        """
        return self.phiAtIntegPointRadiationSet



    def __getstate__(self):

        state = {}
        state["gradPhiAtIntegPoint"] = None
        state["phiAtIntegPoint"] = None
        state["integrationWeights"] = None
        state["listOfTags"] = None
        state["integrationWeightsRadiation"] = None
        state["phiAtIntegPointRadiationSet"] = None

        return state


    def __str__(self):
        res = "OperatorPreCompressionDataThermal"
        return res



if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)



