# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import numpy as np


def ComputeOnline(onlineProblemData, solutionName):
    """
    Compute the online stage using the method of POD on the snapshots and a regression on the coefficients

    The parameters must have been initialized in onlineProblemData

    Parameters
    ----------
    onlineProblemData : ProblemData
        definition of the testing configuration data in a CollectionProblemData object
    solutionName : str
        names of the solution to be treated

    Returns
    -------
    dict
        onlineReducedCoordinates; dictionary with time indices as keys and a np.ndarray of size (numberOfModes,) containing the coefficients of the reduced solution
    """

    regressor = onlineProblemData.GetOnlineData(solutionName)

    onlineParameters = onlineProblemData.GetParametersList()

    onlineCoefficients = regressor.Predict(onlineParameters)

    timeSequence = onlineProblemData.GetParametersTimeSequence()

    onlineReducedCoordinates = {}

    for i, time in enumerate(timeSequence):
        onlineReducedCoordinates[time] = onlineCoefficients[i,:]

    return onlineReducedCoordinates


def CompressOperator(collectionProblemData, regressors):
    """
    Computes the offline operator compression stage using the method of POD on the snapshots and a regression on the coefficients

    Parameters
    ----------
    collectionProblemData : CollectionProblemData
        definition of the training data in a CollectionProblemData object
    regressors : list of regressor OperationCompressionData instances
        regressors defined in Containers/OperationCompressionData/Regressors
    """


    for regressor in regressors:

        solutionName = regressor.GetSolutionName()

        numberOfModes = collectionProblemData.GetReducedOrderBasisNumberOfModes(solutionName)
        numberOfSnapshots = collectionProblemData.GetGlobalNumberOfSnapshots(solutionName)
        parameterDimension = collectionProblemData.GetParameterDimension()

        coefficients = np.zeros((numberOfSnapshots, numberOfModes))
        parameters = np.zeros((numberOfSnapshots, parameterDimension))

        count = 0
        for problemData in collectionProblemData.GetProblemDatas().values():

            localNumberOfSnapshots = problemData.GetSolution(
                solutionName
            ).GetNumberOfSnapshots()

            times = problemData.GetSolution(solutionName).GetTimeSequenceFromReducedCoordinates()

            coefficients[count : count + localNumberOfSnapshots, :] = (
                problemData.GetSolution(solutionName).GetReducedCoordinatesList()
            )

            localParameters = np.array([problemData.GetParameterAtTime(t) for t in times])
            parameters[count : count + localNumberOfSnapshots, :] = localParameters

            count += localNumberOfSnapshots

        regressor.Fit(parameters, coefficients)

        collectionProblemData.AddOperatorCompressionData(regressor)



if __name__ == "__main__":# pragma: no cover

    from genericROM import RunTestFile
    RunTestFile(__file__)


